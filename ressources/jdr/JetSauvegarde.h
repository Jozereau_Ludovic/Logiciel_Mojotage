#ifndef JETSAUVEGARDE_H
#define JETSAUVEGARDE_H

#include <QString>
#include "../utils.h"

class JetSauvegarde {
private:
    int type;
    /* Réflexes, vigueur ou volonté */
    int action;         /* Annule, partiel, 1/2 dégâts ou dévoile */

public:
    JetSauvegarde(int type, int action);

    int getType();

    int getAction();

    QString getType(int type);

    QString getAction(int action);

};

#endif // JETSAUVEGARDE_H
