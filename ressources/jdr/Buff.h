#ifndef BUFF_H
#define BUFF_H

#include "JetSauvegarde.h"
#include <QString>
#include <vector>
#include <memory>


class Buff {
private:
    QString nom;

    int duree;
    int dureeInitiale;
    bool direct;
    /* Indique si le buff s'applique une fois (true) ou à chaque tours (false) */
    bool actif;
    /* Indique si le buff doit être réactivé ou non */

    std::vector<int> caracteristiques;
    std::vector<float> modificateurs;

    std::vector<std::shared_ptr<JetSauvegarde>> jetSauv;    /* Jets de sauvegarde à réussir pour contrer la transmission */

public:
    Buff(QString nom, int duree, bool direct, std::vector<int> caracteristiques,
         std::vector<float> modificateurs,
         std::vector<std::shared_ptr<JetSauvegarde>> jetSauv);

    Buff(std::shared_ptr<Buff> buff);

    QString getNom();

    int getDuree();
    void setDuree(int duree);

    int getDureeInitiale();

    bool getDirect();

    bool getActif();

    void setActif(bool actif);

    std::vector<int> getCarac();

    std::vector<float> getModif();

    std::vector<std::shared_ptr<JetSauvegarde>> getJetSauv();

    void decrementeBuff();

    void update(std::vector<std::shared_ptr<Buff>> buffs);
};

#endif // BUFF_H
