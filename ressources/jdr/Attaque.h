#ifndef LOGICIEL_MOJOTAGE_ATTAQUE_HPP
#define LOGICIEL_MOJOTAGE_ATTAQUE_HPP

#include "Buff.h"
#include "JetSauvegarde.h"
#include <vector>
#include <memory>
#include <QString>

class Attaque {
private:
    /** Identificateurs **/
    int id;

    /** Caractéristiques **/
    QString nom;
    std::vector<float> deDegats;
    /** Dé de dégâts : nombre de dé, type de dé **/
    int degats;
    /** Bonus aux dégâts **/
    int toucher;
    /** Bonus au toucher **/
    int critique;
    /** Valeur minimale du critique **/
    int degatsCritiques;
    /** Multiplicateur de critique **/
    int cooldown;           /** Nombre de tours sans que l'attaque soit disponible après utilisation **/

    /** Divers **/
    int toursRestants;
    /** Nombre de tours restants avant la disponibilité de l'attaque **/
    bool disponible;
    /** Indique si l'attaque est disponible **/
    int detail;

    std::vector<std::shared_ptr<Buff>> buffs;
    /** Liste des buffs liés à l'attaque **/
    std::vector<std::shared_ptr<JetSauvegarde>> jetSauv;   /** Liste des jets de sauvegarde lié à l'attaque **/

public:
    Attaque(int id, QString nom, std::vector<float> deDegats, int degats = 0, int toucher = 0, int critique = 20,
            int degatsCritiques = 2,
            int cooldown = 0, int toursRestants = 0, bool disponible = true,
            int detail = (int) constantes::attaque::AUCUN,
            std::vector<std::shared_ptr<Buff> > buffs = std::vector<std::shared_ptr<Buff>>(),
            std::vector<std::shared_ptr<JetSauvegarde>> jetSauv = std::vector<std::shared_ptr<JetSauvegarde>>());

    /** Getteurs et setteurs **/
    int getId() const;

    QString getNom() const;

    void setNom(QString nom);

    int getDegats() const;

    void setDegats(int degats);

    std::vector<float> getDeDegats() const;

    void setDeDegats(std::vector<float> deDegats);

    void ajouterDeDegats(float deDegats);

    int getToucher() const;

    void setToucher(int toucher);

    int getCritique() const;

    void setCritique(int critique);

    int getDegatsCritiques() const;

    void setDegatsCritiques(int degatsCritiques);

    int getCooldown() const;

    void setCooldown(int cooldown);

    int getToursRestants() const;

    void setToursRestants(int toursRestants);

    bool getDisponible() const;

    void setDisponible(bool disponible);

    int getDetail();

    std::vector<std::shared_ptr<Buff>> getBuffs() const;

    std::vector<std::shared_ptr<JetSauvegarde>> getJetSauv() const;

    void annulerAttaque();

    /** Jets d'attaque **/
    int jetToucher();

    int jetDegats();

    bool coupCritique(int toucher);

    bool echecCritique(int toucher);

    bool estDisponible();

    void decrementeCooldown();

    void chercherDeDegats(float de);
};

#endif //LOGICIEL_MOJOTAGE_ATTAQUE_HPP
