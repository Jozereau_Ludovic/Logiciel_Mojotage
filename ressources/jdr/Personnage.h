#ifndef PERSONNAGE_H
#define PERSONNAGE_H

#include "Buff.h"
#include "Attaque.h"
#include "../utils.h"
#include <vector>
#include <memory>
#include <QString>

class Personnage {
private:
    /*** Identificateurs ***/
    int id;         /* Identifiant */

    /*** Caractéristiques ***/
    QString nom;
    int CA;
    int niveau;
    int vieMax;
    int vie;

    std::vector<std::shared_ptr<Attaque>> attaques;

    int experience;
    int init;
    /* Bonus initiative */
    int reflexes;
    int vigueur;
    int volonte;
    int resistance;

    /*** Divers ***/
    int initiative;
    /* Initiative pour le combat */
    int camp;
    /* ALLIE ou ENNEMI */
    int type;
    /* MOB ou JOUEUR */
    bool vivant;
    bool moitie;

    std::vector<std::shared_ptr<Buff>> buffs;

public:
    /* Constructeur mob */
    Personnage(int id, QString nom, int CA, int niveau, int vieMax, int vie,
               std::vector<std::shared_ptr<Attaque> > attaques, int experience,
               int init = 0, int reflexes = 0, int vigueur = 0, int volonte = 0, int resistance = 0,
               int camp = (int) constantes::camp::ENNEMI,
               std::vector<std::shared_ptr<Buff>> buffs = std::vector<std::shared_ptr<Buff>>());

    /* Constructeur joueur */
    Personnage(int id, QString nom, int CA);

    /*** Getteurs et setteurs ***/
    int getId() const;

    int getCamp() const;

    void setCamp(int camp);

    int getType() const;

    QString getNom() const;

    void setNom(QString nom);

    int getVieMax() const;

    void setVieMax(int vieMax);

    int getVie() const;

    void setVie(int vie);

    int getCA() const;

    void setCA(int CA);

    int getInit() const;

    void setInit(int init);

    int getReflexes() const;

    void setReflexes(int reflexe);

    int getVigueur() const;

    void setVigueur(int vigueur);

    int getVolonte() const;

    void setVolonte(int volonte);

    int getResistance() const;

    void setResistance(int resistance);

    int getInitiative() const;

    void setInitiative(int initiative);

    bool getVivant() const;

    void setVivant(bool vivant);

    bool getMoitie() const;

    void setMoitie(bool moitie);

    std::vector<std::shared_ptr<Attaque>> getAttaques() const;

    void setAttaques(std::vector<std::shared_ptr<Attaque> > attaques);

    std::vector<std::shared_ptr<Buff>> getBuffs() const;

    void setBuffs(std::vector<std::shared_ptr<Buff>> buffs);

    int getExperience();

    /** Gestion des degats subis et infliges **/
    std::vector<std::shared_ptr<Personnage> > attaquer(std::vector<std::shared_ptr<Personnage>> &cibles, std::vector<float> multipVictimes,
                                                       std::shared_ptr<Attaque> &attaque, int toucher, int degats);
    std::vector<std::shared_ptr<Personnage>> attaquerClassique(std::vector<std::shared_ptr<Personnage> > &cibles, std::vector<float> multipVictimes,
                                                               std::shared_ptr<Attaque> &attaque, int toucher, int degats);
    std::vector<std::shared_ptr<Personnage>> attaquerSort(std::vector<std::shared_ptr<Personnage> > &cibles, std::vector<float> multipVictimes,
                                                          std::shared_ptr<Attaque> &attaque, int degats);

    void attaquer(std::vector<std::shared_ptr<Personnage> > &cibles, int toucher, int degats);
    void recevoirDegats(int degats);
    void recevoirSoins(int soins);

    bool estMort();

    /** Gestion des jets de des **/
    int jetInitiative();

    /** Gestion des buffs **/
    void debutTour();

    void activerBuffs();

    void activerBuffsDebutCombat();

    void activerBuff(std::shared_ptr<Buff> &buff);

    void activerBuff(int carac, float modif);

    void transmettreBuff(std::vector<std::shared_ptr<Personnage>> &victimes, std::shared_ptr<Buff> &transmis);

    void ajouterBuff(std::shared_ptr<Buff> buff);

    void nettoyerBuffs(bool debuff = false);

    void annulerBuffs();

    void desactiverBuff(int carac, float modif);

    void decrementeBuffs();

    void decrementeCooldowns();

    /** Gestion des jets de sauvegarde **/
    int degreDifficulte(int type);

    int degreDifficulte(int niveau, int modificateur);

    bool jetSauvegarde(int type, int difficulte);
};

bool triInitiative(const std::shared_ptr<Personnage> perso1, const std::shared_ptr<Personnage> perso2);

bool triCampJoueur(const std::shared_ptr<Personnage> perso1, const std::shared_ptr<Personnage> perso2);

bool triCampMob(const std::shared_ptr<Personnage> perso1, const std::shared_ptr<Personnage> perso2);

bool triVivant(const std::shared_ptr<Personnage> perso1, const std::shared_ptr<Personnage> perso2);

bool triNom(const std::shared_ptr<Personnage> perso1, const std::shared_ptr<Personnage> perso2);

#endif // PERSONNAGE_H
