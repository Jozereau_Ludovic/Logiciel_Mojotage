#ifndef UTILS_H
#define UTILS_H

#include <dirent.h>
#include <QString>
#include <QTextBrowser>

namespace constantes {
    enum class sauvegarde : int {
        REFLEXES, VIGUEUR, VOLONTE
    };
    enum class action : int {
        ANNULE, PARTIEL, MOITIE, DEVOILE
    };
    enum class camp : int {
        ENNEMI, ALLIE
    };
    enum class type : int {
        MOB, JOUEUR
    };
    enum class caracteristique : int {
        VIEMAX, VIE, CA, REFLEXES, VIGUEUR, VOLONTE, RESISTANCE,
        DEGATS, DEDEGATS, TOUCHER, CRITIQUE, DEGCRIT, DEBUFF
    };
    enum class attaque : int {
        AUCUN, SORT, VOLVIE, SOINS, BUFF
    };
/** SORT : CA non prise en compte
 * VOLVIE : moitié des dégâts soignés
 * SOINS : soigne la cible, pas de CA prise en compte
 * BUFF : pas de dégâts ni de CA prise en compte **/
}

int compterFichier(QString chemin);

QString caracBuff(int caracteristique);

QString detailAttaque(int detail);

void afficher(QString donnees);
void afficher(QTextBrowser *texte);

void effacerLigne(QTextBrowser *tableau, int ligne);

#endif // UTILS_H
