#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H

#include <QWidget>
#include <QStackedWidget>

#include "Menu.h"
#include "ressources/fenetres/combat/Joueur.h"
#include "ressources/fenetres/combat/Mob.h"
#include "ChoixJoueurs.h"
#include "ressources/fenetres/combat/TestJetSauv.h"
#include "FinCombat.h"

class FenetrePrincipale : public QWidget {
    Q_OBJECT

private:
    QStackedWidget *fenetres;

    Menu *menu;
    Editeur *editeur;
    ChoixJoueurs *choixJoueurs;

    Joueur *joueur;
    Mob *mob;

    TestJetSauv *testJetSauv;

    FinCombat *finCombat;

    std::vector<std::shared_ptr<Personnage>> personnages;
    int indicePerso;

    std::vector<std::shared_ptr<Personnage>> cibles;
    std::vector<std::shared_ptr<JetSauvegarde>> jetSauvegarde;
    std::vector<float> multiplCibles;
    int indiceJetSauvegarde;

    std::vector<std::shared_ptr<Personnage>> ciblesBuffs;
    std::vector<std::shared_ptr<Buff>> buffs;
    int indiceBuff;

public:
    FenetrePrincipale(QWidget *parent = 0);

    void affichageMob();
    void affichageJoueur();

public slots:
    void afficherEditeur();
    void afficherChoixJoueurs();

    void afficherMenu();

    void afficherCombat();

    void afficherJetSauvegarde(std::vector<std::shared_ptr<Personnage>> cibles,
                               std::vector<std::shared_ptr<JetSauvegarde>> jetSauvegarde,
                               std::vector<float> multiplicateurs);
    void afficherJetSauvegardeSuivant();

    void suivantMob();
    void suivantJoueur();

    void updateCombat();
    void combattre();

    void updateCiblesJetSauv(std::vector<std::shared_ptr<Personnage>>, std::vector<float>);

    void afficherResistanceBuffs(std::vector<std::shared_ptr<Personnage>> victimes, std::vector<std::shared_ptr<Buff>> transmissibles,
                                 std::vector<float> multiplicateurs);
    void afficherResistanceBuffsSuivant();
    void afficherBuffSuivant();
    void transmettreBuff();

    void afficherFin();
};

#endif // FENETREPRINCIPALE_H
