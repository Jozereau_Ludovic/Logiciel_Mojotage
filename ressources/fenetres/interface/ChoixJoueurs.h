#ifndef CHOIXJOUEURS_H
#define CHOIXJOUEURS_H

#include <QWidget>
#include <QVector>
#include <QCheckBox>
#include <QPushButton>
#include <QLineEdit>
#include "../../jdr/Personnage.h"

class ChoixJoueurs : public QWidget {
    Q_OBJECT

private:
    QVector<QCheckBox *> listeJoueurs;
    QVector<QLineEdit *> initJoueurs;
    QPushButton *validation;
    QPushButton *retour;

    std::vector<std::shared_ptr<Personnage>> joueurs;
    std::vector<std::shared_ptr<Personnage>> mobs;

public:
    explicit ChoixJoueurs(std::vector<std::shared_ptr<Personnage>> joueurs,
                          std::vector<std::shared_ptr<Personnage>> mobs, QWidget *parent = 0);

    QPushButton *getValidation();
    QPushButton *getRetour();

    std::vector<std::shared_ptr<Personnage>> valider();
    bool joueursExistants();

    signals:

public slots:
    void disponibilite();
};

#endif // CHOIXJOUEURS_H
