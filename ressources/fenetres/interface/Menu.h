#ifndef MENU_H
#define MENU_H

#include <QWidget>
#include <QPushButton>
#include "../editeur/Editeur.h"
#include "ChoixJoueurs.h"

#include <QStackedWidget>

class Menu : public QWidget {
    Q_OBJECT

private:
    QPushButton *combat;
    QPushButton *editeur;
    QPushButton *quitter;

    QWidget * fenetreSecondaire;

public:
    Menu(QWidget *parent = 0);
    QPushButton *getEditeur();
    QPushButton *getCombat();

    ChoixJoueurs *lancement();

};

#endif // MENU_H
