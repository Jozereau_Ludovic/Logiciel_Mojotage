#ifndef CHOIXCOMBAT_H
#define CHOIXCOMBAT_H

#include <memory>
#include "../../jdr/Personnage.h"

std::vector<std::shared_ptr<Personnage>> creerCombat(QString chemin);

std::shared_ptr<Personnage> creerMob(QString chemin);

std::shared_ptr<Attaque> creerAttaque(QString chemin);

std::shared_ptr<Buff> creerBuff(QString chemin);

std::shared_ptr<JetSauvegarde> creerJetSauv(QString chemin);

std::vector<std::shared_ptr<Personnage>> creerJoueurs(QString chemin);

#endif // CHOIXCOMBAT_H
