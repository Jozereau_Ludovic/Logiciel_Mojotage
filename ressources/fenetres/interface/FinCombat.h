#ifndef FINCOMBAT_H
#define FINCOMBAT_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>

#include <memory>

#include "ressources/jdr/Personnage.h"

class FinCombat : public QWidget {
    Q_OBJECT

private:
    QLabel *experienceTotale;
    QLabel *experienceJoueurs;

    QPushButton *menu;

    std::vector<std::shared_ptr<Personnage>> personnages;
public:
    explicit FinCombat(std::vector<std::shared_ptr<Personnage>> personnages, QWidget *parent = 0);

    QPushButton *getMenu();

    int calculExperience();

    int nombreJoueur();

signals:

public slots:
    void copierFichierSortie();
};

#endif // FINCOMBAT_H
