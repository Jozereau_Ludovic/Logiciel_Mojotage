#ifndef TESTJETSAUV_H
#define TESTJETSAUV_H

#include <QWidget>
#include <QVector>
#include <QVector2D>
#include <QRadioButton>
#include <QPushButton>
#include <QLabel>
#include <memory>
#include <QGroupBox>
#include <QScrollArea>
#include "ressources/utils.h"
#include "ressources/jdr/Personnage.h"

class TestJetSauv : public QWidget {
    Q_OBJECT

private:
    QLabel *degreDiff;
    QLabel *informationsJet;
    QLabel *raisonJet;

    QPushButton *validation;

    QVector<QGroupBox *> groupeResultats;
    QVector<QRadioButton *> resultatsJets;
    QVector<QLabel *> noms;

    std::shared_ptr<Personnage> attaquant;

    QScrollArea * zoneVictimes;
    std::vector<std::shared_ptr<Personnage>> victimes;
    std::vector<float> multiplVictimes;

    std::vector<std::shared_ptr<JetSauvegarde>> jetSauv;
    int indexJet;

public:
    explicit TestJetSauv(std::shared_ptr<Personnage> attaquant, std::vector<std::shared_ptr<Personnage>> victimes,
                         std::vector<float> multiplVictimes, std::vector<std::shared_ptr<JetSauvegarde>> jetSauv,
                         int indexJet = 0, QWidget *parent = 0);

    QPushButton *getValider();

    QLabel *getRaisonJet();

signals:
    void validerResultatJetSauv(std::vector<std::shared_ptr<Personnage>>, std::vector<float>);

public slots:
    void valider();
    std::vector<std::shared_ptr<Personnage>> validerBuffs();
};

#endif // TESTJETSAUV_H
