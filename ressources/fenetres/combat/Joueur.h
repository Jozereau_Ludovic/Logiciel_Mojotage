#ifndef JOUEUR_H
#define JOUEUR_H

#include <QWidget>
#include <QPushButton>
#include <QScrollArea>
#include <QLineEdit>
#include <QCheckBox>
#include <QVector>
#include <QLabel>

#include "../../jdr/Personnage.h"

class Joueur : public QWidget {
    Q_OBJECT

private:
    QLabel *infoDegats;
    QLineEdit *degats;

    QLabel *infoToucher;
    QLineEdit *toucher;

    QScrollArea *zoneCible;

    QPushButton *attaquer;
    QPushButton *suivant;
    QVector<QCheckBox *> cibles;

    QCheckBox *coupCritique;

    std::vector<std::shared_ptr<Personnage>> personnages;
    int numAttaquant;

    QPushButton *mort;
    QPushButton *fin;

    QTextBrowser *sortie;

public:
    Joueur(std::vector<std::shared_ptr<Personnage>> personnages, int numAttaquant = 0, QWidget *parent = 0);

    QPushButton *getSuivant();
    std::vector<std::shared_ptr<Personnage>> getPersonnages();

    void creerZoneCible(std::shared_ptr<Personnage> attaquant);
    void updateAffichageCibles(std::shared_ptr<Personnage> attaquant);

    void resteMobs();

    int passer();

    void mortSuivant();

signals:
    void attaquantMort();
    void combatFini();

public slots:
    void attaque();

    void updateAffichage();

    void finirCombat();

    void afficherGestionMort();
};

#endif // JOUEUR_H
