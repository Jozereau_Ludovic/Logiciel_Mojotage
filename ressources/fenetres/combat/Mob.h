#ifndef MOBATTAQUE_H
#define MOBATTAQUE_H

#include <QWidget>
#include <QPushButton>
#include <QComboBox>
#include <QTextBrowser>
#include <QLabel>
#include <QScrollArea>
#include <QCheckBox>
#include <QSpinBox>
#include <memory>

#include "../../jdr/Personnage.h"
#include "../../jdr/De.h"

class Mob : public QWidget {
    Q_OBJECT

private:
    QPushButton *suivant;
    /* Passage au personnage suivant */
    QPushButton *validation;
    /* Valider l'attaque ou le choix des cibles */
    QPushButton *modif;
    QComboBox *listeAttaques;
    /* Choix de l'attaque */
    QTextBrowser *informations;
    /* Informations sur l'attaque choisie */
    QLabel *infoLancer;
    /* Informations sur le lancer */

    QScrollArea *zoneCible;
    /* Zone des cibles */
    QVector<QCheckBox *> cibles;
    /* Vector des QCheckBox des cibles */
    QVector<QLineEdit *> multiplicateurs;
    /* Vector des multiplicateurs de dégâts reçus des cibles */

    QTextBrowser *general;
    QLabel *infoAttaquant;
    QTextBrowser *buffsActifs;

    QPushButton *mort;

    QPushButton *fin;

    std::vector<std::shared_ptr<Personnage> > personnages;
    int numAttaquant;
    int lancer;

    QTextBrowser *sortie;

public:
    explicit Mob(std::vector<std::shared_ptr<Personnage>> personnages, int numAttaquant = 0, QWidget *parent = 0);

    QPushButton *getSuivant();
    QTextBrowser *getSortie();
    std::vector<std::shared_ptr<Personnage>> getPersonnages();

    void creerZoneCible(std::shared_ptr<Personnage> attaquant);
    void updateAffichageCibles(std::shared_ptr<Personnage> attaquant);

    void updateAffichageBuffs(std::shared_ptr<Personnage> attaquant);

    void resteJoueurs();

    void infoClassique(std::shared_ptr<Attaque> attaque);
    void infoSort(std::shared_ptr<Attaque> attaque);
    void infoBuff(std::shared_ptr<Attaque> attaque);

    int passer();
    int nombrePersonnagesVivants();

    void mortSuivant();

    void combattre(std::vector<std::shared_ptr<Personnage>> victimes, std::vector<float> multiplVictimes);

signals:
    void attaquantMort();
    void combatFini();
    void infoJetSauvegarde(std::vector<std::shared_ptr<Personnage>>, std::vector<std::shared_ptr<JetSauvegarde>>,
                           std::vector<float>);
    void resistanceBuffs(std::vector<std::shared_ptr<Personnage>>, std::vector<std::shared_ptr<Buff>>,
                         std::vector<float>);

public slots:
    void valider();

    void choisirVictimes();

    void info(int index);

    void finirCombat();

    void afficherGestionMort();

    void updateAffichage();
};

#endif // MOBATTAQUE_H
