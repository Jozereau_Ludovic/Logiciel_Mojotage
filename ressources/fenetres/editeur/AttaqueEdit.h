#ifndef ATTAQUEEDIT_H
#define ATTAQUEEDIT_H

#include <QWidget>
#include <QTabWidget>
#include <QPushButton>
#include <QSpinBox>
#include <QLineEdit>
#include <QLabel>
#include <QComboBox>
#include <QTextBrowser>

class AttaqueEdit : public QWidget {
    Q_OBJECT

private:
    QPushButton *ouvrirAttaque;

    QPushButton *ajoutBuff;
    QTextBrowser *infoBuff;
    QTextBrowser *infoBuffLues;
    int nbBuff;
    QPushButton *supprimerBuff;

    QPushButton *ajoutJetSauv;
    QTextBrowser *infoJetSauv;
    QTextBrowser *infoJetSauvLues;
    int nbJetSauv;
    QPushButton *supprimerJetSauv;

    QPushButton *save;

    QLineEdit *nom;
    QLabel *infoNom;

    QLineEdit *nbDe;
    QLineEdit *nbFace;
    QLabel *infoNbDe;
    QLabel *infoTypeDe;

    QVector<QSpinBox *> caracteristiques;
    /* degats, toucher, critique, degatsCritiques, cooldown, toursRestants */
    QVector<QLabel *> infoCaracteristiques;

    QComboBox *detail;
    QLabel *infoDetail;

public:
    explicit AttaqueEdit(QWidget *parent = 0);

    bool attaquePret();

    void reinitialiserAttaque();

public slots:
    void creerAttaque();

    void choixBuff();

    void choixJetSauv();

    void ouvertureAttaque();

    void annulerBuff();

    void annulerJetSauv();

    void adapterDetail(int index);
};


#endif // ATTAQUEEDIT_H
