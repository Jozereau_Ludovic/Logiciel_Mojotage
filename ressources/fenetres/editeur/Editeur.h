#ifndef EDITEUR_H
#define EDITEUR_H

#include <QWidget>
#include <QTabWidget>
#include <QPushButton>
#include <QSpinBox>
#include <QLineEdit>
#include <QLabel>
#include <QComboBox>
#include <QTextBrowser>

class Editeur : public QWidget {
    Q_OBJECT

private:
    QTabWidget *editeur;

    QPushButton *retour;

public:
    explicit Editeur(QWidget *parent = 0);

    QPushButton *getRetour();

    signals:

public
    slots:
};

#endif // EDITEUR_H
