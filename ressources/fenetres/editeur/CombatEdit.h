#ifndef COMBATEDIT_H
#define COMBATEDIT_H

#include <QWidget>
#include <QTabWidget>
#include <QPushButton>
#include <QSpinBox>
#include <QLineEdit>
#include <QLabel>
#include <QComboBox>
#include <QTextBrowser>

class CombatEdit : public QWidget {
    Q_OBJECT

private:
    QPushButton *ouvrirCombat;

    QPushButton *ajoutMob;
    QTextBrowser *infoMob;
    QTextBrowser *infoMobLues;
    QSpinBox *nbMob;
    QLabel *indicMob;

    QPushButton *save;
    QPushButton *supprimerMob;


public:
    explicit CombatEdit(QWidget *parent = 0);

    bool combatPret();

    void reinitialiserCombat();

public
    slots:
    void

    creerCombat();

    void choixMob();

    void ouvertureCombat();
};

#endif // COMBATEDIT_H
