#ifndef JETSAUVEDIT_H
#define JETSAUVEDIT_H

#include <QWidget>
#include <QPushButton>
#include <QComboBox>
#include <QLabel>

class JetSauvEdit : public QWidget {
    Q_OBJECT

private:
    QPushButton *ouvrirJetSauv;

    QPushButton *save;

    QLabel *infoType;
    QLabel *infoAction;
    QComboBox *type;
    QComboBox *action;

public:
    JetSauvEdit(QWidget *parent = 0);

    void reinitialiserJetSauv();

public slots:
    void creerJetSauv();

    void ouvertureJetSauv();

};

#endif // JETSAUVEDIT_H
