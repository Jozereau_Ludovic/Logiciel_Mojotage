#ifndef BUFFEDIT_H
#define BUFFEDIT_H

#include <QWidget>
#include <QTabWidget>
#include <QPushButton>
#include <QSpinBox>
#include <QLineEdit>
#include <QLabel>
#include <QComboBox>
#include <QTextBrowser>

class BuffEdit : public QWidget {
    Q_OBJECT

private:
    QPushButton *ouvrirBuff;

    QPushButton *save;

    QLineEdit *nom;
    QLabel *infoNom;

    QPushButton *ajoutModif;
    QComboBox *caracteristique;
    QLineEdit *modificateur;
    QLineEdit *modificateurDesType;
    QLineEdit *modificateurDesFaces;
    QLabel *infoCarac;
    QTextBrowser *infoModif;
    QTextBrowser *infoModifLues;
    int nbModif;
    QPushButton *supprimerModif;

    QPushButton *ajoutJetSauv;
    QTextBrowser *infoJetSauv;
    QTextBrowser *infoJetSauvLues;
    int nbJetSauv;
    QPushButton *supprimerJetSauv;

    QSpinBox *duree;
    QLabel *infoDuree;

    QComboBox *direct;
    QLabel *infoDirect;


public:
    explicit BuffEdit(QWidget *parent = 0);

    bool buffPret();

    void reinitialiserBuff();

public slots:
    void creerBuff();

    void ouvertureBuff();

    void choixModif();

    void choixJetSauv();

    void annulerModif();

    void annulerJetSauv();

    void adapterCarac(int carac);
};

#endif // BUFFEDIT_H
