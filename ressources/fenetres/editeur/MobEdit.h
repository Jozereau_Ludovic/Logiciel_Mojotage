#ifndef MOBEDIT_H
#define MOBEDIT_H

#include <QWidget>
#include <QTabWidget>
#include <QPushButton>
#include <QSpinBox>
#include <QLineEdit>
#include <QLabel>
#include <QComboBox>
#include <QTextBrowser>

class MobEdit : public QWidget {
    Q_OBJECT

private:
    QPushButton *ouvrirMob;

    QPushButton *ajoutAttaque;
    QTextBrowser *infoAttaque;
    QTextBrowser *infoAttaqueLues;
    int nbAttaque;
    QPushButton *supprimerAttaque;

    QPushButton *ajoutBuff;
    QTextBrowser *infoBuff;
    QTextBrowser *infoBuffLues;
    int nbBuff;
    QPushButton *supprimerBuff;

    QPushButton *save;

    QLineEdit *nom;
    QLabel *infoNom;

    QLineEdit *experience;
    QLabel *infoExperience;

    QVector<QSpinBox *> caracteristiques;
    /* CA, niveau, vieMax, vie, init, reflexes, vigueur, volonte, resistance */
    QVector<QLabel *> infoCaracteristiques;

    QComboBox *camp;
    QLabel *infoCamp;


public:
    explicit MobEdit(QWidget *parent = 0);

    bool mobPret();

    void reinitialiserMob();

public
    slots:
    void

    coherenceVie(int vieMax);

    void creerMob();

    void choixAttaque();

    void choixBuff();

    void ouvertureMob();

    void annulerAttaque();

    void annulerBuff();
};

#endif // MOBEDIT_H
