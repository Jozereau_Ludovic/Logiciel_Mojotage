#ifndef MODIFICATEUR_H
#define MODIFICATEUR_H

#include <QWidget>
#include <QSpinBox>
#include <QPushButton>
#include <QVector>
#include <memory>
#include "../jdr/Personnage.h"

class Modificateur : public QWidget {
    Q_OBJECT

private:
    QPushButton *valider;
    QPushButton *attaques;
    QVector<QSpinBox *> caracteristiques;

    std::shared_ptr<Personnage> mob;


public:
    explicit Modificateur(std::shared_ptr<Personnage> mob, QWidget *parent = 0);

    signals:

public
    slots:
};

#endif // MODIFICATEUR_H
