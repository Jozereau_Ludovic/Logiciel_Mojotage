#-------------------------------------------------
#
# Project created by QtCreator 2016-07-21T11:38:05
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Combat_JDR
TEMPLATE = app


SOURCES += sources/main.cpp\
    sources/jdr/Attaque.cpp \
    sources/jdr/Buff.cpp \
    sources/jdr/De.cpp \
    sources/jdr/Personnage.cpp \
    sources/jdr/JetSauvegarde.cpp \
    sources/fenetres/Modificateur.cpp \
    sources/utils.cpp \
    sources/fenetres/editeur/AttaqueEdit.cpp \
    sources/fenetres/editeur/BuffEdit.cpp \
    sources/fenetres/editeur/CombatEdit.cpp \
    sources/fenetres/editeur/Editeur.cpp \
    sources/fenetres/editeur/MobEdit.cpp \
    sources/fenetres/combat/Joueur.cpp \
    sources/fenetres/combat/Mob.cpp \
    sources/fenetres/combat/TestJetSauv.cpp \
    sources/fenetres/interface/ChoixCombat.cpp \
    sources/fenetres/interface/ChoixJoueurs.cpp \
    sources/fenetres/interface/FenetrePrincipale.cpp \
    sources/fenetres/interface/FinCombat.cpp \
    sources/fenetres/interface/Menu.cpp \
    sources/fenetres/editeur/JetSauvEdit.cpp \
    GestionMort.cpp

HEADERS  += \
    ressources/jdr/Attaque.h \
    ressources/jdr/Buff.h \
    ressources/jdr/De.h \
    ressources/jdr/Personnage.h \
    ressources/utils.h \
    ressources/jdr/JetSauvegarde.h \
    ressources/fenetres/Modificateur.h \
    ressources/fenetres/editeur/AttaqueEdit.h \
    ressources/fenetres/editeur/BuffEdit.h \
    ressources/fenetres/editeur/CombatEdit.h \
    ressources/fenetres/editeur/Editeur.h \
    ressources/fenetres/editeur/MobEdit.h \
    ressources/fenetres/FenetrePrincipale.h \
    ressources/fenetres/combat/Joueur.h \
    ressources/fenetres/combat/Mob.h \
    ressources/fenetres/combat/TestJetSauv.h \
    ressources/fenetres/interface/ChoixCombat.h \
    ressources/fenetres/interface/ChoixJoueurs.h \
    ressources/fenetres/interface/FenetrePrincipale.h \
    ressources/fenetres/interface/FinCombat.h \
    ressources/fenetres/interface/Menu.h \
    ressources/fenetres/editeur/JetSauvEdit.h \
    GestionMort.h

CONFIG += mobility
MOBILITY =

DISTFILES += \
    Combats/Attaques/Forte \
    Combats/Attaques/Simple \
    Combats/Mobs/Gobelin \
    Combats/A_lire \
    Combats/Joueurs \
    Combats/Test1 \
    Combats/Attaques/A_Lire \
    Combats/Attaques/AugmentationPuissance \
    Combats/Attaques/PluieAcide \
    Combats/Buffs/Attaque/Acide \
    Combats/Buffs/NonAttaque/Armure \
    Combats/Buffs/NonAttaque/Puissance \
    Combats/Buffs/A_Lire \
    Combats/JetSauv/A_Lire \
    Combats/JetSauv/ReflexesMoitie \
    Combats/JetSauv/VigueurAnnule \
    Combats/Mobs/A_Lire \
    Combats/Mobs/Sorcier \
    READ_ME \
    Combats/A_Lire/A_lire_Attaques \
    Combats/A_Lire/A_lire_Buffs \
    Combats/A_Lire/A_lire_Combats \
    Combats/A_Lire/A_lire_JetSauv \
    Combats/A_Lire/A_lire_Mobs \
    Combats/Attaques/Coup_de_bec \
    Combats/Attaques/Engloutir \
    Combats/Buffs/Acide \
    Combats/Buffs/Armure \
    Combats/Buffs/Puissance \
    Combats/Combats/Test1 \
    Combats/Combats/Test2 \
    Combats/Combats/Test3 \
    Combats/Joueurs/Joueurs \
    Combats/Mobs/Poulet \
    Combats/Mobs/Slime \
    Combats/Sortie \
    CMakeLists.txt \
    Combats/Mobs/Rocher \
    Combats/Logs/LogCombat0

