#include "../ressources/fenetres/combat/Joueur.h"
#include "../ressources/fenetres/interface//Menu.h"
#include "../ressources/jdr/De.h"
#include "../ressources/fenetres/interface/FenetrePrincipale.h"

#include <QApplication>
#include <QFileDialog>

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    QWidget *fenetre;

    srand(time(0));

    fenetre = new FenetrePrincipale();
    fenetre->showMaximized();

    return a.exec();
}
