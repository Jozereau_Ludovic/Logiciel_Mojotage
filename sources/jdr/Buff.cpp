#include "../../ressources/jdr/Buff.h"

Buff::Buff(QString nom, int duree, bool direct, std::vector<int> caracteristiques,
           std::vector<float> modificateurs, std::vector<std::shared_ptr<JetSauvegarde>> jetSauv) :
        nom(nom), duree(duree), direct(direct), caracteristiques(caracteristiques),
        modificateurs(modificateurs), jetSauv(jetSauv) {
    actif = false;
    dureeInitiale = duree;

}

Buff::Buff(std::shared_ptr<Buff> buff) {
    nom = buff->getNom();
    duree = buff->getDuree();
    dureeInitiale = buff->getDureeInitiale();
    direct = buff->getDirect();
    caracteristiques = buff->getCarac();
    modificateurs = buff->getModif();
    jetSauv = buff->getJetSauv();
    actif = false;
}

QString Buff::getNom() {
    return nom;
}

int Buff::getDuree() {
    return duree;
}

int Buff::getDureeInitiale() {
    return dureeInitiale;
}

bool Buff::getDirect() {
    return direct;
}
void Buff::setDuree(int duree) {
    Buff::duree = duree;
}

bool Buff::getActif() {
    return actif;
}

void Buff::setActif(bool actif) {
    Buff::actif = actif;
}

std::vector<int> Buff::getCarac() {
    return caracteristiques;
}

std::vector<float> Buff::getModif() {
    return modificateurs;
}

std::vector<std::shared_ptr<JetSauvegarde>> Buff::getJetSauv() {
    return jetSauv;
}

void Buff::update(std::vector<std::shared_ptr<Buff>> buffs) {
    for (auto i = buffs.begin(); i != buffs.end();) {
        if (--duree == 0) {
            buffs.erase(i);
        }
        ++i;
    }
}

void Buff::decrementeBuff() {
    --duree;
}
