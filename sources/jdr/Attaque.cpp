#include "../../ressources/jdr/Attaque.h"
#include "../../ressources/jdr/De.h"

Attaque::Attaque(int id, QString nom, std::vector<float> deDegats, int degats, int toucher, int critique,
                 int degatsCritiques,
                 int cooldown, int toursRestants, bool disponible, int detail, std::vector<std::shared_ptr<Buff>> buffs,
                 std::vector<std::shared_ptr<JetSauvegarde>> jetSauv) :
        id(id), nom(nom), deDegats(deDegats), degats(degats), toucher(toucher), critique(critique),
        degatsCritiques(degatsCritiques),
        cooldown(cooldown), toursRestants(toursRestants), disponible(disponible), detail(detail), buffs(buffs),
        jetSauv(jetSauv) {

}

/** Getteurs et setteurs **/
int Attaque::getId() const {
    return id;
}

QString Attaque::getNom() const {
    return nom;
}

void Attaque::setNom(QString nom) {
    Attaque::nom = nom;
}

int Attaque::getDegats() const {
    return degats;
}

void Attaque::setDegats(int degats) {
    Attaque::degats = degats;
}

std::vector<float> Attaque::getDeDegats() const {
    return deDegats;
}

void Attaque::setDeDegats(std::vector<float> deDegats) {
    Attaque::deDegats = deDegats;
}

void Attaque::ajouterDeDegats(float deDegats) {
    Attaque::deDegats.push_back(deDegats);
}

int Attaque::getToucher() const {
    return toucher;
}

void Attaque::setToucher(int toucher) {
    Attaque::toucher = toucher;
}

int Attaque::getCritique() const {
    return critique;
}

void Attaque::setCritique(int critique) {
    Attaque::critique = critique;
}

int Attaque::getDegatsCritiques() const {
    return degatsCritiques;
}

void Attaque::setDegatsCritiques(int degatsCritiques) {
    Attaque::degatsCritiques = degatsCritiques;
}

int Attaque::getCooldown() const {
    return cooldown;
}

void Attaque::setCooldown(int cooldown) {
    Attaque::cooldown = cooldown;
}

int Attaque::getToursRestants() const {
    return toursRestants;
}

void Attaque::setToursRestants(int toursRestants) {
    Attaque::toursRestants = toursRestants;
}

bool Attaque::getDisponible() const {
    return disponible;
}

void Attaque::setDisponible(bool disponible) {
    Attaque::disponible = disponible;
}

int Attaque::getDetail() {
    return detail;
}

std::vector<std::shared_ptr<Buff>> Attaque::getBuffs() const {
    return buffs;
}

std::vector<std::shared_ptr<JetSauvegarde>> Attaque::getJetSauv() const {
    return jetSauv;
}

/** Jets d'attaque **/
int Attaque::jetToucher() {
    return de(20) + toucher;
}

int Attaque::jetDegats() {
    int degatsInfliges = degats, nbDe, nbFaces;

    for (auto i = deDegats.begin(); i != deDegats.end(); ++i) {
        nbDe = floor(*i);
        nbFaces = typeDe(*i);

        for (int i = 0; i < nbDe; ++i) {
            degatsInfliges += de(nbFaces);
        }
    }

    disponible = false;
    toursRestants = cooldown;

    return degatsInfliges;
}

void Attaque::annulerAttaque() {
    toursRestants = 0;
    disponible = true;
}

bool Attaque::coupCritique(int toucher) {
    return (toucher - Attaque::toucher >= critique);
}

bool Attaque::echecCritique(int toucher) {
    return (toucher - Attaque::toucher == 1);
}

bool Attaque::estDisponible() {
    disponible = (toursRestants <= 0);

    return disponible;
}

void Attaque::decrementeCooldown() {
    toursRestants--;
    estDisponible();
}

void Attaque::chercherDeDegats(float de) {
    bool trouve = false;

    for (auto i = deDegats.begin(); i != deDegats.end() && !trouve; ++i) {
        if ((*i) == de) {
            trouve = true;
            deDegats.erase(i);
        }
    }
}
