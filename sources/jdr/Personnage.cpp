#include "../../ressources/jdr/Personnage.h"
#include "../../ressources/jdr/De.h"
#include <iostream>

/*** Constructeurs ***/
Personnage::Personnage(int id, QString nom, int CA, int niveau, int vieMax, int vie,
                       std::vector<std::shared_ptr<Attaque>> attaques, int experience,
                       int init, int reflexes, int vigueur, int volonte, int resistance,
                       int camp, std::vector<std::shared_ptr<Buff>> buffs) :
    id(id), nom(nom), CA(CA), niveau(niveau), vieMax(vieMax), vie(vie), attaques(attaques), experience(experience),
    init(init), reflexes(reflexes), vigueur(vigueur), volonte(volonte), resistance(resistance),
    camp(camp), buffs(buffs) {
    type = (int) constantes::type::MOB;
    initiative = jetInitiative();
    vivant = true;
    moitie = false;

}

Personnage::Personnage(int id, QString nom, int CA) :
    id(id), nom(nom), CA(CA) {
    vie = 100000;
    camp = (int) constantes::camp::ALLIE;
    type = (int) constantes::type::JOUEUR;
    vivant = true;
    niveau = 1;
    moitie = false;
}

/*** Getteurs et setteurs ***/
int Personnage::getId() const {
    return id;
}

int Personnage::getCamp() const {
    return camp;
}

void Personnage::setCamp(int camp) {
    Personnage::camp = camp;
}

int Personnage::getType() const {
    return type;
}

QString Personnage::getNom() const {
    return nom;
}

void Personnage::setNom(QString nom) {
    Personnage::nom = nom;
}

int Personnage::getVieMax() const {
    return vieMax;
}

void Personnage::setVieMax(int vieMax) {
    Personnage::vieMax = vieMax;
}

int Personnage::getVie() const {
    return vie;
}

void Personnage::setVie(int vie) {
    Personnage::vie = vie;
}

int Personnage::getCA() const {
    return CA;
}

void Personnage::setCA(int CA) {
    Personnage::CA = CA;
}

int Personnage::getInit() const {
    return init;
}

void Personnage::setInit(int init) {
    Personnage::init = init;
}

int Personnage::getReflexes() const {
    return reflexes;
}

void Personnage::setReflexes(int reflexes) {
    Personnage::reflexes = reflexes;
}

int Personnage::getVigueur() const {
    return vigueur;
}

void Personnage::setVigueur(int vigueur) {
    Personnage::vigueur = vigueur;
}

int Personnage::getVolonte() const {
    return volonte;
}

void Personnage::setVolonte(int volonte) {
    Personnage::volonte = volonte;
}

int Personnage::getResistance() const {
    return resistance;
}

void Personnage::setResistance(int resistance) {
    Personnage::resistance = resistance;
}

int Personnage::getInitiative() const {
    return initiative;
}

void Personnage::setInitiative(int initiative) {
    Personnage::initiative = initiative;
}

bool Personnage::getVivant() const {
    return vivant;
}

void Personnage::setVivant(bool vivant) {
    Personnage::vivant = vivant;
}

bool Personnage::getMoitie() const {
    return moitie;
}

void Personnage::setMoitie(bool moitie) {
    Personnage::moitie = moitie;
}

std::vector<std::shared_ptr<Attaque> > Personnage::getAttaques() const {
    return attaques;
}

void Personnage::setAttaques(std::vector<std::shared_ptr<Attaque>> attaques) {
    Personnage::attaques = attaques;
}

std::vector<std::shared_ptr<Buff>> Personnage::getBuffs() const {
    return buffs;
}

void Personnage::setBuffs(std::vector<std::shared_ptr<Buff>> buffs) {
    Personnage::buffs = buffs;
}

int Personnage::getExperience() {
    return experience;
}

/*** Gestion des degats subis et infliges ***/
std::vector<std::shared_ptr<Personnage>> Personnage::attaquer(std::vector<std::shared_ptr<Personnage> > &cibles, std::vector<float> multipVictimes,
                                                              std::shared_ptr<Attaque> &attaque, int toucher, int degats) {
    std::vector<std::shared_ptr<Personnage>> ciblesTouchees = std::vector<std::shared_ptr<Personnage>>();

    switch (attaque->getDetail()) {
    case (int) constantes::attaque::AUCUN:
        ciblesTouchees = attaquerClassique(cibles, multipVictimes, attaque, toucher, degats);
        break;
    case (int) constantes::attaque::BUFF:
        afficher(this->nom + " lance un buff.");
        ciblesTouchees = cibles;
        break;
    case (int) constantes::attaque::SORT:
        ciblesTouchees = attaquerSort(cibles, multipVictimes, attaque, degats);
        break;
    }

    return ciblesTouchees;
}

std::vector<std::shared_ptr<Personnage>> Personnage::attaquerClassique(std::vector<std::shared_ptr<Personnage> > &cibles, std::vector<float> multipVictimes,
                                                                       std::shared_ptr<Attaque> &attaque, int toucher, int degats) {
    int degatsTotaux = 0;
    bool echecCritique = attaque->echecCritique(toucher), coupCritique = attaque->coupCritique(toucher);
    std::vector<std::shared_ptr<Personnage>> ciblesTouchees = std::vector<std::shared_ptr<Personnage>>();

    for (unsigned int i = 0; i < cibles.size(); ++i) {
        afficher(this->nom + " attaque " + cibles[i]->getNom() + ".");

        if (echecCritique) {
            cibles[i]->recevoirDegats(degats * multipVictimes[i]); /** On force les dégâts car c'est qu'on voulait les donner */
            degatsTotaux += degats * multipVictimes[i];
            ciblesTouchees.push_back(cibles[i]);
        }

        else if (!coupCritique && toucher < cibles[i]->getCA()) {
            afficher("\tAttaque échouée contre " + cibles[i]->getNom() + ".\n");
        }

        else if (coupCritique && toucher > cibles[i]->getCA()) {
            afficher("\tCoup critique !");
            cibles[i]->recevoirDegats(degats * multipVictimes[i] * attaque->getCritique());
            degatsTotaux += degats * multipVictimes[i] * attaque->getCritique();
            ciblesTouchees.push_back(cibles[i]);
        }
        else if (!coupCritique && toucher > cibles[i]->getCA()) {
            cibles[i]->recevoirDegats(degats * multipVictimes[i]);
            degatsTotaux += degats * multipVictimes[i];
            ciblesTouchees.push_back(cibles[i]);
        }

        if (attaque->getDetail() == (int) constantes::attaque::VOLVIE)
            this->recevoirSoins(degatsTotaux/2);
    }
    return ciblesTouchees;
}

std::vector<std::shared_ptr<Personnage>> Personnage::attaquerSort(std::vector<std::shared_ptr<Personnage> > &cibles, std::vector<float> multipVictimes,
                                                                  std::shared_ptr<Attaque> &attaque, int degats) {
    for (unsigned int i = 0; i < cibles.size(); ++i) {
        afficher(this->nom + " lance un sort sur " + cibles[i]->getNom() + ".");

        if (attaque->getDetail() == (int) constantes::attaque::SOINS) {
            afficher("\t" + this->nom + " soigne " + cibles[i]->getNom() + ".");
            cibles[i]->recevoirSoins(degats);
        }
        else {
            cibles[i]->recevoirDegats(degats * multipVictimes[i]);
        }
    }
    return cibles;
}

void Personnage::attaquer(std::vector<std::shared_ptr<Personnage>> &cibles, int toucher, int degats) {
    for (auto i = cibles.begin(); i != cibles.end(); i++) {
        afficher(this->nom + " attaque " + (*i)->getNom() + ".");

        if (toucher >= (*i)->getCA()) {
            (*i)->recevoirDegats(degats);
        }
    }
}

void Personnage::recevoirDegats(int degats) {
    if (moitie) {
        degats = degats/2;
        moitie = false;
    }

    degats = degats - resistance;

    if (degats < 0) {
        degats = 0;
    }

    vie = vie - degats;

    if (vie < 0) {
        vie = 0;
    }

    if (this->type != (int) constantes::type::JOUEUR) {
        afficher("\t" + this->nom + " perd " + QString::number(degats) + " pv.");
    }
    else afficher("\t" + this->nom + " perd " + QString::number(degats) + " pv.\n");

    if (this->type != (int) constantes::type::JOUEUR) {
        if (estMort()) {
            afficher("\t" + this->nom + " est mort.\n");
        }
        else {
            afficher("\t" + QString::number(this->vie) + " pv restants à " + this->nom + ".\n");
        }
    }
}

void Personnage::recevoirSoins(int soins) {
    vie += soins;

    if (vie > vieMax) {
        vie = vieMax;
    }

    if (this->type != (int) constantes::type::JOUEUR) {
        afficher("\t" + this->nom + " gagne " + QString::number(soins) + " pv.");
        afficher("\t" + QString::number(this->vie) + " pv restants à " + this->nom + ".\n");
    }
    else  afficher("\t" + this->nom + " gagne " + QString::number(soins) + " pv.\n");
}

bool Personnage::estMort() {
    if (vie <= 0) {
        vivant = false;
        vie = 0;
        annulerBuffs();
    }
    else {
        vivant = true;
    }
    return !vivant;
}

/*** Gestion des jets de des ***/
int Personnage::jetInitiative() {
    initiative = de(20) + init;
    return initiative;
}

/** Gestion des buffs **/
void Personnage::debutTour() {
    afficher("\nDébut du tour de " + this->nom);
    activerBuffs();
    decrementeBuffs();
    decrementeCooldowns();

}

void Personnage::activerBuffs() {
    for (auto i = buffs.begin(); i != buffs.end(); ++i) {
        activerBuff(*i);
    }
    if (estMort()) {
        afficher(this->nom + " est mort.");
    }
}

void Personnage::activerBuffsDebutCombat() {
    for (auto i = buffs.begin(); i != buffs.end(); ++i) {
        if ((*i)->getDirect()) {
            activerBuff(*i);
        }
    }
}

void Personnage::activerBuff(std::shared_ptr<Buff> &buff) {
    if ((buff->getDirect() && !buff->getActif()) || !buff->getDirect()) {
        for (unsigned int i = 0; i < buff->getCarac().size(); ++i) {
            activerBuff(buff->getCarac()[i], buff->getModif()[i]);

            if (buff->getCarac()[i] != (int) constantes::caracteristique::DEBUFF) {
                afficher("\t" + this->nom + " : " + QString::number(buff->getModif()[i])
                         + " " + caracBuff(buff->getCarac()[i]));
            }
        }
        afficher("");
        buff->setActif(true);
    }
}

/*** Modification des caractéristiques par les buffs ***/
void Personnage::activerBuff(int carac, float modif) {
    switch (carac) {
    case (int) constantes::caracteristique::VIEMAX:
        vieMax += modif;
    case (int) constantes::caracteristique::VIE:
        vie += modif;
        break;
    case (int) constantes::caracteristique::CA:
        CA += modif;
        break;
    case (int) constantes::caracteristique::REFLEXES:
        reflexes += modif;
        break;
    case (int) constantes::caracteristique::VIGUEUR:
        vigueur += modif;
        break;
    case (int) constantes::caracteristique::VOLONTE:
        volonte += modif;
        break;
    case (int) constantes::caracteristique::RESISTANCE:
        resistance += modif;
        break;
    case (int) constantes::caracteristique::DEGATS:
        for (auto i = attaques.begin(); i != attaques.end(); ++i) {
            (*i)->setDegats((*i)->getDegats() + modif);
        }
        break;
    case (int) constantes::caracteristique::DEDEGATS:
        for (auto i = attaques.begin(); i != attaques.end(); ++i) {
            (*i)->ajouterDeDegats(modif);
        }
        break;
    case (int) constantes::caracteristique::TOUCHER:
        for (auto i = attaques.begin(); i != attaques.end(); ++i) {
            (*i)->setToucher((*i)->getToucher() + modif);
        }
        break;
    case (int) constantes::caracteristique::CRITIQUE:
        for (auto i = attaques.begin(); i != attaques.end(); ++i) {
            (*i)->setCritique((*i)->getCritique() - modif);
        }
        break;
    case (int) constantes::caracteristique::DEGCRIT:
        for (auto i = attaques.begin(); i != attaques.end(); ++i) {
            (*i)->setDegatsCritiques((*i)->getDegatsCritiques() + modif);
        }
        break;
    case (int) constantes::caracteristique::DEBUFF:
        if (modif == -1) {
            for (auto i = buffs.begin(); i != buffs.end(); ++i) {
                (*i)->setDuree(0);
            }
        }
        else {
            for (auto i = buffs.begin(); i != buffs.end(); ++i) {
                int newDuree = (*i)->getDuree() - modif;

                if (newDuree < 0) {
                    newDuree = 0;
                }
                (*i)->setDuree(newDuree);
            }
        }
        nettoyerBuffs(true);
    default:
        break;
    }
}

void Personnage::transmettreBuff(std::vector<std::shared_ptr<Personnage>> &victimes, std::shared_ptr<Buff> &transmis) {
    for (auto i = victimes.begin(); i != victimes.end(); ++i) {
        if ((*i)->getVivant()) {
            afficher((*i)->getNom() + " reçoit " + transmis->getNom());
            (*i)->ajouterBuff(transmis);

            if (transmis->getDirect()) {
                (*i)->activerBuff((*i)->getBuffs().back());
            }
        }
    }
}

void Personnage::ajouterBuff(std::shared_ptr<Buff> buff) {
    std::shared_ptr<Buff> copie = std::make_shared<Buff>(buff);
    buffs.push_back(copie);
}

void Personnage::nettoyerBuffs(bool debuff) {
    for (auto i = buffs.begin(); i != buffs.end();) {
        if ((*i)->getDuree() == 0) {
            if (!debuff) {
                afficher("\t" + this->nom + " n'est plus affecté par " + (*i)->getNom());
            }
            if ((*i)->getDirect()) {
                for (unsigned int j = 0; j < (*i)->getCarac().size(); ++j) {
                    desactiverBuff((*i)->getCarac()[j], (*i)->getModif()[j]);
                }
            }
            else {
                for (unsigned int j = 0; j < (*i)->getCarac().size(); ++j) {
                    desactiverBuff((*i)->getCarac()[j], (*i)->getModif()[j]*(*i)->getDureeInitiale());
                }
            }
            buffs.erase(i);
            afficher("");
        }
        else {
            ++i;
        }
    }
}

void Personnage::annulerBuffs() {
    for (auto i = buffs.begin(); i != buffs.end();) {
        afficher("\t" + this->nom + " n'est plus affecté par " + (*i)->getNom());

        if ((*i)->getActif()) {
            if ((*i)->getDirect()) {
                for (unsigned int j = 0; j < (*i)->getCarac().size(); ++j) {
                    desactiverBuff((*i)->getCarac()[j], (*i)->getModif()[j]);
                }
            }
            else {
                for (unsigned int j = 0; j < (*i)->getCarac().size(); ++j) {
                    desactiverBuff((*i)->getCarac()[j], (*i)->getModif()[j]*((*i)->getDureeInitiale()-(*i)->getDuree() + 1));
                }
            }
        }
        buffs.erase(i);
        afficher("");
    }
}

void Personnage::desactiverBuff(int carac, float modif) {
    if (carac != (int) constantes::caracteristique::DEBUFF) {
        if (carac == (int) constantes::caracteristique::VIEMAX) {
            vieMax -= modif;

            if (vie > vieMax) {
                vie = vieMax;
            }
        }
        else if (carac == (int) constantes::caracteristique::DEDEGATS) {
            for (auto i = attaques.begin(); i != attaques.end(); ++i) {
                (*i)->chercherDeDegats(modif);
            }
        }
        else if (carac != (int) constantes::caracteristique::VIE) {
            afficher("\t" + this->nom + ": " + QString::number(-modif) +  " " + caracBuff(carac));
            activerBuff(carac, -modif);
        }
    }
}

void Personnage::decrementeBuffs() {
    for (auto i = buffs.begin(); i != buffs.end(); ++i) {
        (*i)->decrementeBuff();
    }
}

void Personnage::decrementeCooldowns() {
    for (auto i = attaques.begin(); i != attaques.end(); ++i) {
        (*i)->decrementeCooldown();
    }
}


/** Gestion des jets de sauvegarde **/
/* Pour les mobs */
int Personnage::degreDifficulte(int type) {
    int difficulte =  10 + niveau;

    switch (type) {
    case (int) constantes::sauvegarde::REFLEXES:
        difficulte += reflexes;
        break;
    case (int) constantes::sauvegarde::VIGUEUR:
        difficulte += vigueur;
        break;
    case (int) constantes::sauvegarde::VOLONTE:
        difficulte += volonte;
        break;
    }

    return difficulte;
}

/* Pour les joueurs */
int Personnage::degreDifficulte(int niveau, int modificateur) {
    return 10 + niveau + modificateur;
}

bool Personnage::jetSauvegarde(int type, int difficulte) {
    int lancer = de(20);

    switch (type) {
    case (int) constantes::sauvegarde::REFLEXES:
        lancer += reflexes;
        break;
    case (int) constantes::sauvegarde::VIGUEUR:
        lancer += vigueur;
        break;
    case (int) constantes::sauvegarde::VOLONTE:
        lancer += volonte;
        break;
    }

    return lancer > difficulte;
}

/*** Fonction de tri ***/
bool triInitiative(const std::shared_ptr<Personnage> perso1, const std::shared_ptr<Personnage> perso2) {
    return (perso1->getInitiative() > perso2->getInitiative());
}

bool triCampJoueur(const std::shared_ptr<Personnage> perso1, const std::shared_ptr<Personnage> perso2) {
    return (perso1->getCamp() > perso2->getCamp());
}

bool triCampMob(const std::shared_ptr<Personnage> perso1, const std::shared_ptr<Personnage> perso2) {
    return (perso1->getCamp() < perso2->getCamp());
}

bool triVivant(const std::shared_ptr<Personnage> perso1, const std::shared_ptr<Personnage> perso2) {
    return (perso1->getVivant() > perso2->getVivant());
}

bool triNom(const std::shared_ptr<Personnage> perso1, const std::shared_ptr<Personnage> perso2) {
    return (perso1->getNom() > perso2->getNom());
}
