#include "../../ressources/jdr/JetSauvegarde.h"

JetSauvegarde::JetSauvegarde(int type, int action) :
        type(type), action(action) {

}

int JetSauvegarde::getType() {
    return type;
}

int JetSauvegarde::getAction() {
    return action;
}

QString JetSauvegarde::getType(int type) {
    QString sortie;
    switch (type) {
        case (int) constantes::sauvegarde::REFLEXES:
            sortie = "Réflexes";
        break;
        case (int) constantes::sauvegarde::VIGUEUR:
            sortie = "Vigueur";
        break;
        case (int) constantes::sauvegarde::VOLONTE:
            sortie = "Volonté";
        break;
    }

    return sortie;
}

QString JetSauvegarde::getAction(int action) {
    QString sortie;

    switch (action) {
        case (int) constantes::action::ANNULE:
            sortie = "Annule";
        break;
        case (int) constantes::action::DEVOILE:
            sortie = "Dévoile";
        break;
        case (int) constantes::action::MOITIE:
            sortie = "Moitié";
        break;
        case (int) constantes::action::PARTIEL:
            sortie = "Partiel";
        break;
    }

    return sortie;
}
