#include "../../ressources/jdr/De.h"
#include <stdlib.h>
#include <cmath>

int de(int n) {
    int resultat = rand() % (n - 1) + 1;

    return resultat;
}

int jetChance() {
    return de(100);
}

int typeDe(float deDegats) {
    float nbFace = (deDegats - std::floor(deDegats)) * 100;

    return std::round(nbFace);
}

float convDeDegats(int nbDe, int typeDe) {
    return nbDe + (float) (typeDe) / 100;
}
