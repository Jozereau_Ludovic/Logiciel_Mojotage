#include <QLayout>
#include <QGroupBox>
#include "GestionMort.h"
#include "ressources/fenetres/combat/Mob.h"
#include "ressources/fenetres/combat/Joueur.h"

Mob::Mob(std::vector<std::shared_ptr<Personnage>> personnages, int numAttaquant, QWidget *parent) : QWidget(parent),
    personnages(personnages), numAttaquant(numAttaquant), lancer(de(20)) {
    std::shared_ptr<Personnage> attaquant = personnages[numAttaquant];

    QGridLayout *layout = new QGridLayout();

    attaquant->debutTour();

    infoAttaquant = new QLabel(attaquant->getNom() + ": " + QString::number(attaquant->getVie())
                               + "/" + QString::number(attaquant->getVieMax()), this);
    layout->addWidget(infoAttaquant, 1, 0);

    buffsActifs = new QTextBrowser();
    layout->addWidget(buffsActifs, 1, 2);
    this->updateAffichageBuffs(attaquant);

    listeAttaques = new QComboBox(this);
    std::vector<std::shared_ptr<Attaque>> attaques = attaquant->getAttaques();
    for (auto i = attaques.begin(); i != attaques.end(); ++i) {
        listeAttaques->addItem((*i)->getNom());
    }
    layout->addWidget(listeAttaques, 2, 0);

    suivant = new QPushButton("Suivant", this);
    layout->addWidget(suivant, 3, 3);

    validation = new QPushButton("Valider", this);
    layout->addWidget(validation, 2, 2);

    modif = new QPushButton("Modifier", this);
    layout->addWidget(modif, 0, 0);

    informations = new QTextBrowser(this);
    this->info(listeAttaques->currentIndex());
    layout->addWidget(informations, 3, 0);

    zoneCible = new QScrollArea();
    this->creerZoneCible(attaquant);
    layout->addWidget(zoneCible, 3, 1);

    infoLancer = new QLabel(this);
    layout->addWidget(infoLancer, 3, 2);
    infoLancer->hide();

    sortie = new QTextBrowser(this);
    layout->addWidget(sortie, 0, 4, 4, 1);
    afficher(sortie);

    mort = new QPushButton("Gérer les morts", this);
    layout->addWidget(mort);

    fin = new QPushButton("Finir le combat", this);
    layout->addWidget(fin);

    this->setLayout(layout);

    QObject::connect(validation, SIGNAL(clicked()), this, SLOT(valider()));
    QObject::connect(listeAttaques, SIGNAL(highlighted(int)), this, SLOT(info(int)));
    QObject::connect(modif, SIGNAL(clicked()), this, SLOT(modifier()));
    QObject::connect(mort, SIGNAL(clicked()), this, SLOT(afficherGestionMort()));
    QObject::connect(fin, SIGNAL(clicked()), this, SLOT(finirCombat()));
}

QPushButton *Mob::getSuivant() {
    return suivant;
}

QTextBrowser *Mob::getSortie() {
    return sortie;
}

std::vector<std::shared_ptr<Personnage>> Mob::getPersonnages() {
    return personnages;
}

int Mob::passer() {
    int newAttaquant = (numAttaquant + 1) % personnages.size();

    while (!personnages[newAttaquant]->getVivant()) {
        newAttaquant = (newAttaquant + 1) % personnages.size();
    }

    personnages[numAttaquant]->nettoyerBuffs();

    return newAttaquant;
}

void Mob::valider() {
    std::shared_ptr<Personnage> attaquant = personnages[numAttaquant];
    std::shared_ptr<Attaque> attaque = attaquant->getAttaques()[listeAttaques->currentIndex()];
    bool lancerNormal = false;

    listeAttaques->setEnabled(false);
    informations->setEnabled(false);
    suivant->setEnabled(false);
    zoneCible->setEnabled(true);
    validation->setText(detailAttaque(attaque->getDetail()));

    if (lancer == 1) {
        infoLancer->setText("Echec critique !\nChance: " + QString::number(jetChance()));
    }
    else if (lancer >= attaque->getCritique()) {
        infoLancer->setText("Coup critique !\nLancer: " + QString::number(lancer));
    }
    else {
        infoLancer->setText("Lancer : " + QString::number(lancer));
        lancerNormal = true;
    }

    if (!lancerNormal) {
        infoLancer->show();
    }

    afficher(sortie);

    QObject::disconnect(validation, SIGNAL(clicked()), this, SLOT(valider()));
    QObject::connect(validation, SIGNAL(clicked()), this, SLOT(choisirVictimes()));
    QObject::connect(mort, SIGNAL(clicked()), this, SLOT(afficherGestionMort()));
}

void Mob::choisirVictimes() {
    std::vector<std::shared_ptr<Personnage>> victimes = std::vector<std::shared_ptr<Personnage>>();
    std::vector<float> multiplVictimes = std::vector<float>();
    std::shared_ptr<Personnage> attaquant = personnages[numAttaquant];
    std::shared_ptr<Attaque> attaque = attaquant->getAttaques()[listeAttaques->currentIndex()];
    bool coherent = true;

    std::sort(personnages.begin(), personnages.end(), triVivant);
    std::sort(personnages.begin(), personnages.end(), triCampJoueur); /** Même tri que les cibles **/

    for (auto i = 0; i < cibles.size(); ++i) {
        if (cibles[i]->isChecked()) {
            victimes.push_back(personnages[i]);
            if (multiplicateurs[i]->text() != 0) {
                multiplVictimes.push_back(multiplicateurs[i]->text().toFloat());
                multiplicateurs[i]->setStyleSheet("QLineEdit { background: white }");
            }
            else {
                coherent = false;
                multiplicateurs[i]->setStyleSheet("QLineEdit { background: red }");
            }
        }
        else {
            multiplicateurs[i]->setStyleSheet("QLineEdit { background: white }");
        }
    }

    if (coherent) {
        emit infoJetSauvegarde(victimes, attaque->getJetSauv(), multiplVictimes);
    }

    std::sort(personnages.begin(), personnages.end(), triNom);
    std::sort(personnages.begin(), personnages.end(), triInitiative);

    afficher(sortie);
}

void Mob::combattre(std::vector<std::shared_ptr<Personnage>> victimes, std::vector<float> multiplVictimes) {
    std::sort(personnages.begin(), personnages.end(), triNom);
    std::sort(personnages.begin(), personnages.end(), triInitiative);

    std::shared_ptr<Personnage> attaquant = personnages[numAttaquant];
    std::shared_ptr<Attaque> attaque = attaquant->getAttaques()[listeAttaques->currentIndex()];
    int toucher = lancer + attaque->getToucher(), degats = attaque->jetDegats();

    std::sort(personnages.begin(), personnages.end(), triVivant);
    std::sort(personnages.begin(), personnages.end(), triCampJoueur); /** Même tri que les cibles **/

    if (victimes.empty()) {
        attaque->annulerAttaque();
    }

    victimes = attaquant->attaquer(victimes, multiplVictimes, attaque, toucher, degats);

    for (auto i = victimes.begin(); i != victimes.end();) {
        if ((*i)->estMort()) {
            victimes.erase(i);
        }
        else ++i;
    }

    std::sort(personnages.begin(), personnages.end(), triNom);
    std::sort(personnages.begin(), personnages.end(), triInitiative);

    if (attaquant->estMort()) {
        emit attaquantMort();
    }
    else {
        lancer = de(20);
        listeAttaques->setEnabled(true);
        informations->setEnabled(true);

        this->updateAffichageCibles(attaquant);
        this->updateAffichageBuffs(attaquant);

        validation->setText("Valider");
        suivant->setEnabled(true);
        infoLancer->hide();

        listeAttaques->setCurrentIndex(0);
        validation->setEnabled(attaquant->getAttaques()[0]->estDisponible());
        info(listeAttaques->currentIndex());

        QObject::disconnect(validation, SIGNAL(clicked()), this, SLOT(choisirVictimes()));
        QObject::connect(validation, SIGNAL(clicked()), this, SLOT(valider()));
    }

    afficher(sortie);

    emit resistanceBuffs(victimes, attaque->getBuffs(), multiplVictimes);
}

void Mob::info(int index) {
    std::shared_ptr<Attaque> attaque = personnages[numAttaquant]->getAttaques()[index];

    informations->clear();
    validation->setEnabled(attaque->estDisponible());

    switch (attaque->getDetail()) {
    case (int) constantes::attaque::AUCUN:
    case (int) constantes::attaque::VOLVIE:
        infoClassique(attaque);
        break;
    case (int) constantes::attaque::SORT:
    case (int) constantes::attaque::SOINS:
        infoSort(attaque);
        break;
    case (int) constantes::attaque::BUFF:
        infoBuff(attaque);
    }
}

void Mob::infoClassique(std::shared_ptr<Attaque> attaque) {
    if (attaque->getDegats() != 0) {
        informations->setText("Dégâts: " + QString::number(std::floor(attaque->getDeDegats()[0]))
                + "d" + QString::number(typeDe(attaque->getDeDegats()[0]))
                + " +" + QString::number(attaque->getDegats()));
    }
    else {
        informations->setText("Dégâts: " + QString::number(std::floor(attaque->getDeDegats()[0]))
                + "d" + QString::number(typeDe(attaque->getDeDegats()[0])));
    }
    for (unsigned int i = 1; i < attaque->getDeDegats().size(); ++i) {
        informations->append(QString::number(std::floor(attaque->getDeDegats()[i]))
                             + "d" + QString::number(typeDe(attaque->getDeDegats()[i])));
    }
    informations->append("Toucher: +" + QString::number(attaque->getToucher()));
    informations->append("Critique: " + QString::number(attaque->getCritique()) + "-20 /x" +
                         QString::number(attaque->getDegatsCritiques()));
    if (attaque->getCooldown() > 0) {
        informations->append("Cooldown: " + QString::number(attaque->getCooldown()));
    }
    if (attaque->getToursRestants() > 0) {
        informations->append("Tours restants: " + QString::number(attaque->getToursRestants()));
    }
    for (unsigned int i = 0; i != attaque->getBuffs().size(); ++i) {
        informations->append("\nBuff applicable: " + attaque->getBuffs()[i]->getNom());
        informations->append("\tDurée: " + QString::number(attaque->getBuffs()[i]->getDureeInitiale()));
        for (unsigned int j = 0; j < attaque->getBuffs()[i]->getModif().size(); ++j) {
            informations->append("\t" + caracBuff(attaque->getBuffs()[i]->getCarac()[j]) + " "
                                 + QString::number(attaque->getBuffs()[i]->getModif()[j]));
        }
    }
}

void Mob::infoSort(std::shared_ptr<Attaque> attaque) {
    if (attaque->getDegats() != 0) {
        informations->setText("Dégâts: " + QString::number(std::floor(attaque->getDeDegats()[0]))
                + "d" + QString::number(typeDe(attaque->getDeDegats()[0]))
                + " +" + QString::number(attaque->getDegats()));
    }
    else {
        informations->setText("Dégâts: " + QString::number(std::floor(attaque->getDeDegats()[0]))
                + "d" + QString::number(typeDe(attaque->getDeDegats()[0])));
    }

    if (attaque->getDetail() == (int) constantes::attaque::SOINS) {
        informations->toPlainText().replace("Dégâts", "Soins");
    }
    for (unsigned int i = 1; i < attaque->getDeDegats().size(); ++i) {
        informations->append(QString::number(std::floor(attaque->getDeDegats()[i]))
                             + "d" + QString::number(typeDe(attaque->getDeDegats()[i])));
    }
    if (attaque->getCooldown() > 0) {
        informations->append("Cooldown: " + QString::number(attaque->getCooldown()));
    }
    if (attaque->getToursRestants() > 0) {
        informations->append("Tours restants: " + QString::number(attaque->getToursRestants()));
    }
    for (unsigned int i = 0; i != attaque->getBuffs().size(); ++i) {
        informations->append("\nBuff applicable: " + attaque->getBuffs()[i]->getNom());
        informations->append("\tDurée: " + QString::number(attaque->getBuffs()[i]->getDureeInitiale()));
        for (unsigned int j = 0; j < attaque->getBuffs()[i]->getModif().size(); ++j) {
            informations->append("\t" + caracBuff(attaque->getBuffs()[i]->getCarac()[j]) + " "
                                 + QString::number(attaque->getBuffs()[i]->getModif()[j]));


        }
    }
}

void Mob::infoBuff(std::shared_ptr<Attaque> attaque) {
    if (attaque->getCooldown() > 0) {
        informations->append("Cooldown: " + QString::number(attaque->getCooldown()));
    }
    if (attaque->getToursRestants() > 0) {
        informations->append("Tours restants: " + QString::number(attaque->getToursRestants()));
    }
    for (unsigned int i = 0; i != attaque->getBuffs().size(); ++i) {
        informations->append("\nBuff applicable: " + attaque->getBuffs()[i]->getNom());
        informations->append("\tDurée: " + QString::number(attaque->getBuffs()[i]->getDureeInitiale()));
        for (unsigned int j = 0; j < attaque->getBuffs()[i]->getModif().size(); ++j) {
            informations->append("\t" + caracBuff(attaque->getBuffs()[i]->getCarac()[j]) + " "
                                 + QString::number(attaque->getBuffs()[i]->getModif()[j]));
        }
    }
}

void Mob::creerZoneCible(std::shared_ptr<Personnage> attaquant) {
    QCheckBox *cible;
    QLineEdit *multiplicateur;

    QWidget *scrollWidget = new QWidget(this);

    zoneCible->setWidgetResizable(true);
    zoneCible->setWidget(scrollWidget);

    QGridLayout *layout = new QGridLayout();

    std::sort(personnages.begin(), personnages.end(), triVivant);
    std::sort(personnages.begin(), personnages.end(), triCampJoueur); /** Liste triée avec les joueurs et mobs vivants, puis les morts **/

    for (unsigned int i = 0; i != personnages.size(); ++i) {
        if (personnages[i]->getVivant()) {
            if (personnages[i]->getType() == (int) constantes::type::MOB) {
                cible = new QCheckBox(personnages[i]->getNom() + ": " + QString::number(personnages[i]->getVie())
                                      + "/" + QString::number(personnages[i]->getVieMax()), zoneCible);
            }
            else {
                cible = new QCheckBox(personnages[i]->getNom(), zoneCible);
            }
            multiplicateur = new QLineEdit("1", zoneCible);

            cibles.push_back(cible);
            multiplicateurs.push_back(multiplicateur);

            layout->addWidget(cible, i, 0);
            layout->addWidget(multiplicateur, i, 1);

            if (personnages[i]->getCamp() != attaquant->getCamp()) {
                cible->setStyleSheet("QCheckBox { color: blue }");
            }
            if (personnages[i]->getNom() == attaquant->getNom()) {
                cible->setStyleSheet("QCheckBox { color: red }");
            }
        }
    }
    std::sort(personnages.begin(), personnages.end(), triNom);
    std::sort(personnages.begin(), personnages.end(), triInitiative);
    scrollWidget->setLayout(layout);

    zoneCible->setEnabled(false);
}

void Mob::updateAffichageCibles(std::shared_ptr<Personnage> attaquant) {
    zoneCible->widget()->~QWidget();
    multiplicateurs.clear();
    cibles.clear();
    infoAttaquant->setText(attaquant->getNom() + ": " + QString::number(attaquant->getVie()) + "/" + QString::number(attaquant->getVieMax()));
    creerZoneCible(attaquant);
}

void Mob::resteJoueurs() {
    bool reste = false;

    for (auto i = personnages.begin(); i != personnages.end() && !reste; ++i) {
        if ((*i)->getType() == (int) constantes::type::JOUEUR && (*i)->getVivant()) {
            reste =  true;
        }
    }

    if (!reste) emit combatFini();
}

int Mob::nombrePersonnagesVivants() {
    int vivant = 0;

    for (auto i = personnages.begin(); i != personnages.end(); ++i) {
        if ((*i)->getVivant()) {
            vivant++;
        }
    }
    return vivant;
}

void Mob::mortSuivant() {
    if (personnages[numAttaquant]->estMort()) {
        emit attaquantMort();
    }
}

void Mob::updateAffichageBuffs(std::shared_ptr<Personnage> attaquant) {
    buffsActifs->clear();

    if (attaquant->getBuffs().empty()) {
        buffsActifs->append("Pas de buffs actifs");
    }
    else {
        for (unsigned int i = 0; i != attaquant->getBuffs().size(); ++i) {
            buffsActifs->append(attaquant->getBuffs()[i]->getNom());
            if (attaquant->getBuffs()[i]->getDuree() >= 0) {
                buffsActifs->append("\tDurée: " + QString::number(attaquant->getBuffs()[i]->getDuree()));
            }
            for (unsigned int j = 0; j < attaquant->getBuffs()[i]->getModif().size(); ++j) {
                buffsActifs->append("\t" + caracBuff(attaquant->getBuffs()[i]->getCarac()[j]) + " "
                                    + QString::number(attaquant->getBuffs()[i]->getModif()[j]));
            }
        }
    }
}

void Mob::finirCombat() {
    emit combatFini();
}

void Mob::afficherGestionMort() {
    GestionMort *gestion = new GestionMort(personnages, this);

    gestion->show();

    QObject::connect(gestion, SIGNAL(affichage()), this, SLOT(updateAffichage()));
}

void Mob::updateAffichage() {
    updateAffichageCibles(personnages[numAttaquant]);
    updateAffichageBuffs(personnages[numAttaquant]);
}
