#include "ressources/fenetres/combat/Joueur.h"
#include "ressources/fenetres/combat/Mob.h"
#include "GestionMort.h"

#include <QVBoxLayout>

Joueur::Joueur(std::vector<std::shared_ptr<Personnage>> personnages, int numAttaquant, QWidget *parent) : QWidget(parent),
    personnages(personnages), numAttaquant(numAttaquant) {
    std::shared_ptr<Personnage> attaquant = personnages[numAttaquant];

    QGridLayout *layout = new QGridLayout();

    attaquant->debutTour();
    resteMobs();

    infoDegats = new QLabel("Dégâts", this);
    degats = new QLineEdit(this);
    layout->addWidget(infoDegats, 0, 0);
    layout->addWidget(degats, 1, 0);

    infoToucher = new QLabel("Toucher", this);
    toucher = new QLineEdit(this);
    layout->addWidget(infoToucher, 0, 1);
    layout->addWidget(toucher, 1, 1);

    zoneCible = new QScrollArea(this);
    this->creerZoneCible(attaquant);
    layout->addWidget(zoneCible, 2, 0);

    attaquer = new QPushButton("Attaquer", this);
    layout->addWidget(attaquer, 2, 1);

    suivant = new QPushButton("Suivant", this);
    layout->addWidget(suivant, 2, 2);

    sortie = new QTextBrowser(this);
    layout->addWidget(sortie, 0, 3, 4, 1);

    mort = new QPushButton("Gérer les morts", this);
    layout->addWidget(mort);

    fin = new QPushButton("Finir le combat", this);
    layout->addWidget(fin);

    this->setLayout(layout);

    afficher(sortie);

    QObject::connect(attaquer, SIGNAL(clicked()), this, SLOT(attaque()));
    QObject::connect(mort, SIGNAL(clicked()), this, SLOT(afficherGestionMort()));
    QObject::connect(fin, SIGNAL(clicked()), this, SLOT(finirCombat()));
}

QPushButton *Joueur::getSuivant() {
    return suivant;
}

std::vector<std::shared_ptr<Personnage>> Joueur::getPersonnages() {
    return personnages;
}

void Joueur::creerZoneCible(std::shared_ptr<Personnage> attaquant) {
    QCheckBox *cible;

    QWidget *scrollWidget = new QWidget(this);

    zoneCible->setWidgetResizable(true);
    zoneCible->setWidget(scrollWidget);

    QGridLayout *layout = new QGridLayout();

    std::sort(personnages.begin(), personnages.end(), triVivant);
    std::sort(personnages.begin(), personnages.end(), triCampMob); /** Liste triée avec les joueurs et mobs vivants, puis les morts **/

    for (unsigned int i = 0; i != personnages.size(); ++i) {
        if (personnages[i]->getVivant()) {
            if (personnages[i]->getType() == (int) constantes::type::MOB) {
                cible = new QCheckBox(personnages[i]->getNom() + ": " + QString::number(personnages[i]->getVie())
                                      + "/" + QString::number(personnages[i]->getVieMax()), zoneCible);
            }
            else {
                cible = new QCheckBox(personnages[i]->getNom(), zoneCible);
            }

            cibles.push_back(cible);
            layout->addWidget(cible, i, 0);

            if (personnages[i]->getCamp() != attaquant->getCamp()) {
                cible->setStyleSheet("QCheckBox { color: blue }");
            }
            if (personnages[i]->getNom() == attaquant->getNom()) {
                cible->setStyleSheet("QCheckBox { color: red }");
            }
        }
    }
    std::sort(personnages.begin(), personnages.end(), triNom);
    std::sort(personnages.begin(), personnages.end(), triInitiative);
    scrollWidget->setLayout(layout);
}

void Joueur::updateAffichageCibles(std::shared_ptr<Personnage> attaquant) {
    zoneCible->widget()->~QWidget();
    cibles.clear();
    creerZoneCible(attaquant);

    afficher(sortie);

}

int Joueur::passer() {
    int newAttaquant = (numAttaquant + 1) % personnages.size();

    while (!personnages[newAttaquant]->getVivant()) {
        newAttaquant = (newAttaquant + 1) % personnages.size();
    }

    personnages[numAttaquant]->nettoyerBuffs();

    return newAttaquant;
}

void Joueur::attaque() {
    std::vector<std::shared_ptr<Personnage>> victimes;
    std::shared_ptr<Personnage> attaquant = personnages[numAttaquant];

    std::sort(personnages.begin(), personnages.end(), triVivant);
    std::sort(personnages.begin(), personnages.end(), triCampMob);

    for (auto i = 0; i < cibles.size(); ++i) {
        if (cibles[i]->isChecked()) {
            victimes.push_back(personnages[i]);
        }
    }

    attaquant->attaquer(victimes, toucher->text().toInt(), degats->text().toInt());

    toucher->clear();
    degats->clear();

    for (auto i = victimes.begin(); i != victimes.end(); ++i) {
        (*i)->estMort();
    }

    std::sort(personnages.begin(), personnages.end(), triNom);
    std::sort(personnages.begin(), personnages.end(), triInitiative);

    if (!attaquant->estMort()) {
        this->updateAffichageCibles(attaquant);
    }
    else {
        emit attaquantMort();
    }

    afficher(sortie);
}

void Joueur::resteMobs() {
    bool reste = false;

    for (auto i = personnages.begin(); i != personnages.end() && !reste; ++i) {
        if ((*i)->getType() == (int) constantes::type::MOB && (*i)->getVivant()) {
            reste =  true;
        }
    }

    if (!reste) emit combatFini();
}

void Joueur::mortSuivant() {
    if (personnages[numAttaquant]->estMort()) {
        emit attaquantMort();
    }
}

void Joueur::finirCombat() {
    emit combatFini();
}

void Joueur::afficherGestionMort() {
    GestionMort *gestion = new GestionMort(personnages, this);

    gestion->show();

    QObject::connect(gestion, SIGNAL(affichage()), this, SLOT(updateAffichage()));
}

void Joueur::updateAffichage() {
    updateAffichageCibles(personnages[numAttaquant]);
}
