#include "../../../ressources/fenetres/combat/TestJetSauv.h"
#include <QLayout>

TestJetSauv::TestJetSauv(std::shared_ptr<Personnage> attaquant, std::vector<std::shared_ptr<Personnage> > victimes,
                         std::vector<float> multiplVictimes, std::vector<std::shared_ptr<JetSauvegarde>> jetSauv,
                         int indexJet, QWidget *parent) :
    QWidget(parent), attaquant(attaquant), victimes(victimes), multiplVictimes(multiplVictimes), jetSauv(jetSauv), indexJet(indexJet) {
    QGridLayout *layout = new QGridLayout();

    std::shared_ptr<JetSauvegarde> jetSauvegarde = jetSauv[indexJet];
    int difficulte = attaquant->degreDifficulte(jetSauvegarde->getType());

    raisonJet = new QLabel(this);
    layout->addWidget(raisonJet, 0, 0);

    degreDiff = new QLabel("DD: " + QString::number(difficulte), this);
    layout->addWidget(degreDiff, 1, 1);

    informationsJet = new QLabel("Jet de " + jetSauvegarde->getType(jetSauvegarde->getType()).toLower() + " - "
                                 + jetSauvegarde->getAction(jetSauvegarde->getAction()));
    layout->addWidget(informationsJet, 1, 0);

    zoneVictimes = new QScrollArea();
    QGridLayout *scrollLayout = new QGridLayout();
    QWidget *scrollWidget = new QWidget(this);

    zoneVictimes->setWidgetResizable(true);
    zoneVictimes->setWidget(scrollWidget);
    layout->addWidget(zoneVictimes, 2, 0);

    for (auto i = victimes.begin(); i != victimes.end(); ++i) {
        QLabel *nom = new QLabel((*i)->getNom(), this);
        noms.push_back(nom);

        QGroupBox *groupeResultat = new QGroupBox(this);
        QHBoxLayout *layoutGroupe = new QHBoxLayout();
        QRadioButton *reussi = new QRadioButton("Réussi", groupeResultat);
        QRadioButton *echoue = new QRadioButton("Echoué", groupeResultat);

        layoutGroupe->addWidget(reussi);
        layoutGroupe->addWidget(echoue);
        groupeResultat->setLayout(layoutGroupe);

        if ((*i)->getType() == (int) constantes::type::MOB &&
                !(*i)->jetSauvegarde(jetSauvegarde->getType(), difficulte)) {
            echoue->setChecked(true);
        }
        else {
            reussi->setChecked(true);
        }
        resultatsJets.push_back(reussi);
        resultatsJets.push_back(echoue);
        groupeResultats.push_back(groupeResultat);
    }

    for (int i = 0; i < noms.size(); ++i) {
        scrollLayout->addWidget(noms[i], i, 0);
        scrollLayout->addWidget(groupeResultats[i], i, 1);
    }
    scrollWidget->setLayout(scrollLayout);

    validation = new QPushButton("Valider", this);
    layout->addWidget(validation, 3, 0);

    setLayout(layout);
}

QPushButton *TestJetSauv::getValider() {
    return validation;
}

QLabel *TestJetSauv::getRaisonJet() {
    return raisonJet;
}

void TestJetSauv::valider() {
    validation->setEnabled(false);

    std::shared_ptr<JetSauvegarde> jetSauvegarde = jetSauv[indexJet];
    std::vector<int> effaceur = std::vector<int>();

    for (unsigned int i = 0; i < victimes.size(); ++i) {
        if (resultatsJets[2*i]->isChecked()) {
            if (jetSauvegarde->getAction() == (int) constantes::action::MOITIE) {
                victimes[i]->setMoitie(true);
            }
            else {
                effaceur.push_back(i);
            }
            afficher(victimes[i]->getNom() + " à réussi le jet de "
                     + jetSauvegarde->getType(jetSauvegarde->getType()).toLower() + ".");
        }
        else {
            afficher(victimes[i]->getNom() + " à échoué le jet de "
                     + jetSauvegarde->getType(jetSauvegarde->getType()).toLower() + ".");
        }
    }
    afficher("");

    while (!effaceur.empty()) {
        victimes.erase(victimes.begin() + effaceur.back());
        multiplVictimes.erase(multiplVictimes.begin() + effaceur.back());
        effaceur.pop_back();
    }

    emit validerResultatJetSauv(victimes, multiplVictimes);
    validation->setEnabled(true);
}

std::vector<std::shared_ptr<Personnage>> TestJetSauv::validerBuffs() {
    validation->setEnabled(false);

    std::shared_ptr<JetSauvegarde> jetSauvegarde = jetSauv[indexJet];

    std::vector<int> effaceur = std::vector<int>();

    for (unsigned int i = 0; i < victimes.size(); ++i) {
        if (resultatsJets[2*i]->isChecked()) {
            effaceur.push_back(i);
            afficher(victimes[i]->getNom() + " à réussi le jet de "
                     + jetSauvegarde->getType(jetSauvegarde->getType()).toLower() + ".");
        }
        else {
            afficher(victimes[i]->getNom() + " à échoué le jet de "
                     + jetSauvegarde->getType(jetSauvegarde->getType()).toLower() + ".");
        }
    }
    afficher("");

    while (!effaceur.empty()) {
        victimes.erase(victimes.begin() + effaceur.back());
        effaceur.pop_back();
    }

    validation->setEnabled(true);

    return victimes;
}
