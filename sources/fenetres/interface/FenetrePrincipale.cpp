#include "../../../ressources/fenetres/interface/FenetrePrincipale.h"

#include <QLayout>

FenetrePrincipale::FenetrePrincipale(QWidget *parent) : QWidget(parent) {
    QHBoxLayout *layout = new QHBoxLayout();
    indicePerso = 0;
    indiceJetSauvegarde = 0;
    indiceBuff = 0;

    menu = new Menu(this);
    editeur = new Editeur(this);

    fenetres = new QStackedWidget(this);
    fenetres->addWidget(menu);
    fenetres->addWidget(editeur);
    layout->addWidget(fenetres);

    this->setLayout(layout);

    QObject::connect(menu->getEditeur(), SIGNAL(clicked()), this, SLOT(afficherEditeur()));
    QObject::connect(menu->getCombat(), SIGNAL(clicked()), this, SLOT(afficherChoixJoueurs()));
    QObject::connect(editeur->getRetour(), SIGNAL(clicked()), this, SLOT(afficherMenu()));
}

void FenetrePrincipale::afficherEditeur() {
    fenetres->setCurrentWidget(editeur);
}

void FenetrePrincipale::afficherChoixJoueurs() {
    choixJoueurs = menu->lancement();

    if (choixJoueurs != 0) {
        fenetres->addWidget(choixJoueurs);

        fenetres->setCurrentWidget(choixJoueurs);

        QObject::connect(choixJoueurs->getValidation(), SIGNAL(clicked()), this, SLOT(afficherCombat()));
        QObject::connect(choixJoueurs->getRetour(), SIGNAL(clicked()), this, SLOT(afficherMenu()));
    }
}

void FenetrePrincipale::afficherMenu() {
    fenetres->setCurrentWidget(menu);
}

void FenetrePrincipale::afficherCombat() {
    personnages = choixJoueurs->valider(); /** Liste des personnages triée par initiative **/

    if (!personnages.empty()) {
        if (personnages[indicePerso]->getType() == (int) constantes::type::JOUEUR) {
            affichageJoueur();
        }

        else {
            affichageMob();
        }
    }
}

void FenetrePrincipale::suivantMob() {
    indicePerso = mob->passer();
    personnages = mob->getPersonnages();

    if (personnages[indicePerso]->getType() == (int) constantes::type::JOUEUR) {
        affichageJoueur();
    }

    else {
        affichageMob();
    }
}

void FenetrePrincipale::suivantJoueur() {
    indicePerso = joueur->passer();
    personnages = joueur->getPersonnages();

    if (personnages[indicePerso]->getType() == (int) constantes::type::JOUEUR) {
        affichageJoueur();
    }

    else {
        affichageMob();
    }
}

void FenetrePrincipale::affichageMob() {
    mob = new Mob(personnages, indicePerso, this);
    fenetres->addWidget(mob);
    fenetres->setCurrentWidget(mob);

    QObject::connect(mob->getSuivant(), SIGNAL(clicked()), this, SLOT(suivantMob()));
    QObject::connect(mob, SIGNAL(infoJetSauvegarde(std::vector<std::shared_ptr<Personnage>>, std::vector<std::shared_ptr<JetSauvegarde>>, std::vector<float>)),
                     this, SLOT(afficherJetSauvegarde(std::vector<std::shared_ptr<Personnage>>, std::vector<std::shared_ptr<JetSauvegarde>>, std::vector<float>)));
    QObject::connect(mob, SIGNAL(attaquantMort()), this, SLOT(suivantMob()));
    QObject::connect(mob, SIGNAL(combatFini()), this, SLOT(afficherFin()));

    mob->mortSuivant();
    mob->resteJoueurs();
}

void FenetrePrincipale::affichageJoueur() {
    joueur = new Joueur(personnages, indicePerso, this);
    fenetres->addWidget(joueur);
    fenetres->setCurrentWidget(joueur);

    QObject::connect(joueur->getSuivant(), SIGNAL(clicked()), this, SLOT(suivantJoueur()));
    QObject::connect(joueur, SIGNAL(attaquantMort()), this, SLOT(suivantJoueur()));
    QObject::connect(joueur, SIGNAL(combatFini()), this, SLOT(afficherFin()));

    joueur->mortSuivant();
    joueur->resteMobs();
}

void FenetrePrincipale::afficherJetSauvegarde(std::vector<std::shared_ptr<Personnage>> victimes,
                                              std::vector<std::shared_ptr<JetSauvegarde>> jetSauv,
                                              std::vector<float> multiplicateurs) {
    cibles = victimes;
    jetSauvegarde = jetSauv;
    multiplCibles = multiplicateurs;

    indiceBuff = 0;
    indiceJetSauvegarde = 0;

    if (!jetSauv.empty() && !victimes.empty()) {
        testJetSauv = new TestJetSauv(personnages[indicePerso], cibles, multiplCibles, jetSauvegarde, indiceJetSauvegarde, this);
        fenetres->addWidget(testJetSauv);
        fenetres->setCurrentWidget(testJetSauv);
        testJetSauv->getRaisonJet()->setText("Jet de sauvegarde d'attaque");

        /** Si c'est le seul jet de sauvegarde **/
        if ((unsigned int) ++indiceJetSauvegarde == jetSauvegarde.size()) {
            QObject::connect(testJetSauv->getValider(), SIGNAL(clicked()), this, SLOT(combattre()));
        }
        else {
            QObject::connect(testJetSauv->getValider(), SIGNAL(clicked()), this, SLOT(afficherJetSauvegardeSuivant()));
        }
        QObject::connect(testJetSauv, SIGNAL(validerResultatJetSauv(std::vector<std::shared_ptr<Personnage>>, std::vector<float>)),
                         this, SLOT(updateCiblesJetSauv(std::vector<std::shared_ptr<Personnage>>, std::vector<float>)));
    }
    else {
        QObject::connect(mob, SIGNAL(resistanceBuffs(std::vector<std::shared_ptr<Personnage>>,std::vector<std::shared_ptr<Buff>>, std::vector<float>)),
                         this, SLOT(afficherResistanceBuffs(std::vector<std::shared_ptr<Personnage>>,std::vector<std::shared_ptr<Buff>>, std::vector<float>)));

        mob->combattre(cibles, multiplCibles);
    }
}

void FenetrePrincipale::afficherJetSauvegardeSuivant() {
    testJetSauv->valider();

    if (!cibles.empty()) {
        testJetSauv = new TestJetSauv(personnages[indicePerso], cibles, multiplCibles, jetSauvegarde, indiceJetSauvegarde, this);
        fenetres->addWidget(testJetSauv);
        fenetres->setCurrentWidget(testJetSauv);

        /** Si c'est le dernier jet de sauvegarde **/
        if ((unsigned int) ++indiceJetSauvegarde == jetSauvegarde.size()) {
            QObject::connect(testJetSauv->getValider(), SIGNAL(clicked()), this, SLOT(combattre()));
        }
        else {
            QObject::connect(testJetSauv->getValider(), SIGNAL(clicked()), this, SLOT(afficherJetSauvegardeSuivant()));
        }
        QObject::connect(testJetSauv, SIGNAL(validerResultatJetSauv(std::vector<std::shared_ptr<Personnage>>, std::vector<float>)),
                         this, SLOT(updateCiblesJetSauv(std::vector<std::shared_ptr<Personnage>>, std::vector<float>)));
    }
    else {
        QObject::connect(mob, SIGNAL(resistanceBuffs(std::vector<std::shared_ptr<Personnage>>,std::vector<std::shared_ptr<Buff>>, std::vector<float>)),
                         this, SLOT(afficherResistanceBuffs(std::vector<std::shared_ptr<Personnage>>,std::vector<std::shared_ptr<Buff>>, std::vector<float>)));

        mob->combattre(cibles, multiplCibles);
    }
}

void FenetrePrincipale::updateCombat() {
    mob->updateAffichageCibles(personnages[indicePerso]);
    mob->updateAffichageBuffs(personnages[indicePerso]);
    afficher(mob->getSortie());

    indiceJetSauvegarde = 0;
    indiceBuff = 0;

    fenetres->setCurrentWidget(mob);
}

void FenetrePrincipale::combattre() {
    testJetSauv->valider();

    QObject::connect(mob, SIGNAL(resistanceBuffs(std::vector<std::shared_ptr<Personnage>>,std::vector<std::shared_ptr<Buff>>, std::vector<float>)),
                     this, SLOT(afficherResistanceBuffs(std::vector<std::shared_ptr<Personnage>>,std::vector<std::shared_ptr<Buff>>, std::vector<float>)));

    mob->combattre(cibles, multiplCibles);
}

void FenetrePrincipale::updateCiblesJetSauv(std::vector<std::shared_ptr<Personnage>> victimes, std::vector<float> multiplicateurs) {
    cibles = victimes;
    multiplCibles = multiplicateurs;
}

/** **************************************/
void FenetrePrincipale::afficherResistanceBuffs(std::vector<std::shared_ptr<Personnage>> victimes ,std::vector<std::shared_ptr<Buff>> transmissibles, std::vector<float> multiplicateurs) {
    indiceJetSauvegarde = 0;

    buffs = transmissibles;
    cibles = victimes;
    ciblesBuffs = cibles;
    multiplCibles = multiplicateurs;

    QObject::disconnect(mob, SIGNAL(resistanceBuffs(std::vector<std::shared_ptr<Personnage>>,std::vector<std::shared_ptr<Buff>>, std::vector<float>)),
                        this, SLOT(afficherResistanceBuffs(std::vector<std::shared_ptr<Personnage>>,std::vector<std::shared_ptr<Buff>>, std::vector<float>)));

    if (!buffs.empty() && !ciblesBuffs.empty()) {
        jetSauvegarde = buffs[indiceBuff]->getJetSauv();

        if (!jetSauvegarde.empty()) {
            testJetSauv = new TestJetSauv(personnages[indicePerso], ciblesBuffs, multiplCibles, jetSauvegarde, indiceJetSauvegarde, this);
            fenetres->addWidget(testJetSauv);
            fenetres->setCurrentWidget(testJetSauv);
            testJetSauv->getRaisonJet()->setText("Jet de sauvegarde de buff");

            if ((unsigned int) ++indiceJetSauvegarde == jetSauvegarde.size()) {
                if ((unsigned int) indiceBuff + 1 == buffs.size()) {
                    QObject::connect(testJetSauv->getValider(), SIGNAL(clicked()), this, SLOT(transmettreBuff()));
                }
                else {
                    indiceBuff++;
                    QObject::connect(testJetSauv->getValider(), SIGNAL(clicked()), this, SLOT(afficherBuffSuivant()));
                }
            }
            else {
                QObject::connect(testJetSauv->getValider(), SIGNAL(clicked()), this, SLOT(afficherResistanceBuffsSuivant()));
            }
        }
        else {
            if ((unsigned int) indiceBuff + 1 == buffs.size()) {
                personnages[indicePerso]->transmettreBuff(ciblesBuffs, buffs[indiceBuff]);
                updateCombat();
            }
            else {
                indiceBuff++;
                afficherBuffSuivant();
            }
        }
    }
    else {
        updateCombat();
    }
}

void FenetrePrincipale::afficherResistanceBuffsSuivant() {
    ciblesBuffs = testJetSauv->validerBuffs();

    if (!ciblesBuffs.empty()) {
        testJetSauv = new TestJetSauv(personnages[indicePerso], ciblesBuffs, multiplCibles, jetSauvegarde, indiceJetSauvegarde, this);
        fenetres->addWidget(testJetSauv);
        fenetres->setCurrentWidget(testJetSauv);

        if ((unsigned int) ++indiceJetSauvegarde == jetSauvegarde.size()) {
            if ((unsigned int) indiceBuff + 1 == buffs.size()) {
                QObject::connect(testJetSauv->getValider(), SIGNAL(clicked()), this, SLOT(transmettreBuff()));
            }
            else {
                indiceBuff++;
                QObject::connect(testJetSauv->getValider(), SIGNAL(clicked()), this, SLOT(afficherBuffSuivant()));
            }
        }
        else {
            QObject::connect(testJetSauv->getValider(), SIGNAL(clicked()), this, SLOT(afficherResistanceBuffsSuivant()));
        }
    }
    else {
        updateCombat();
    }
}

void FenetrePrincipale::afficherBuffSuivant() {
    personnages[indicePerso]->transmettreBuff(ciblesBuffs, buffs[indiceBuff]);
    afficherResistanceBuffs(cibles, buffs, multiplCibles);
}

void FenetrePrincipale::transmettreBuff() {
    ciblesBuffs = testJetSauv->validerBuffs();

    personnages[indicePerso]->transmettreBuff(ciblesBuffs, buffs[indiceBuff]);
    updateCombat();
}

void FenetrePrincipale::afficherFin() {
    finCombat = new FinCombat(personnages, this);
    fenetres->addWidget(finCombat);
    fenetres->setCurrentWidget(finCombat);

    QObject::connect(finCombat->getMenu(), SIGNAL(clicked()), this, SLOT(afficherMenu()));
}
