#include "ressources/fenetres/interface/FinCombat.h"
#include "ressources/utils.h"

#include <QLayout>
#include <QFile>
#include <QTextStream>

FinCombat::FinCombat(std::vector<std::shared_ptr<Personnage>> personnages, QWidget *parent) :
    QWidget(parent), personnages(personnages) {
    QGridLayout *layout = new QGridLayout();

    menu = new QPushButton("Retourner au menu", this);
    layout->addWidget(menu, 1, 1);

    experienceTotale = new QLabel("Expérience totale du combat: " +
                                  QString::number(calculExperience()), this);
    layout->addWidget(experienceTotale, 0, 0);
    experienceJoueurs = new QLabel("Expérience par joueurs: " +
                                   QString::number(calculExperience()/nombreJoueur()), this);
    layout->addWidget(experienceJoueurs, 0, 2);

    setLayout(layout);

    QObject::connect(menu, SIGNAL(clicked()), this, SLOT(copierFichierSortie()));
}

QPushButton *FinCombat::getMenu() {
    return menu;
}

int FinCombat::calculExperience() {
    int experience = 0;

    for (auto i = personnages.begin(); i != personnages.end(); ++i) {
        if ((*i)->getType() == (int) constantes::type::MOB && !(*i)->getVivant()) {
            experience += (*i)->getExperience();
        }
    }

    return experience;
}

int FinCombat::nombreJoueur() {
    int joueurs = 0;

    for (auto i = personnages.begin(); i != personnages.end(); ++i) {
        if ((*i)->getType() == (int) constantes::type::JOUEUR) {
            joueurs++;
        }
    }

    return joueurs;
}

void FinCombat::copierFichierSortie() {
    QFile fichierLecture("/home/jozereau/Combat_JDR/Combats/Sortie"),
            fichierEcriture("/home/jozereau/Combat_JDR/Combats/Logs/LogCombat"
                            + QString::number(compterFichier("/home/jozereau/Combat_JDR/Combats/Logs/")));

    fichierLecture.open(QIODevice::ReadOnly | QIODevice::Text);
    fichierEcriture.open(QIODevice::WriteOnly | QIODevice::Text);

    QTextStream lire(&fichierLecture), ecrire(&fichierEcriture);

    ecrire << lire.readAll();

    fichierLecture.close();
    fichierEcriture.close();
    remove("/home/jozereau/Combat_JDR/Combats/Sortie");
}
