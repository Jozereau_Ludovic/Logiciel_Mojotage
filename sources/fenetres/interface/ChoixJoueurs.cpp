#include "../../../ressources/fenetres/interface/ChoixJoueurs.h"
#include "../../../ressources/fenetres/combat/Mob.h"
#include <QLayout>

ChoixJoueurs::ChoixJoueurs(std::vector<std::shared_ptr<Personnage>> joueurs,
                           std::vector<std::shared_ptr<Personnage>> mobs, QWidget *parent) :
    QWidget(parent), joueurs(joueurs), mobs(mobs) {

    QGridLayout *layout = new QGridLayout(this);
    for (auto i = joueurs.begin(); i != joueurs.end(); ++i) {
        QCheckBox *joueur = new QCheckBox((*i)->getNom(), this);
        QLineEdit *init = new QLineEdit(this);

        listeJoueurs.push_back(joueur);
        initJoueurs.push_back(init);

        joueur->setChecked(true);
    }

    for (int i = 0; i < listeJoueurs.size(); ++i) {
        layout->addWidget(listeJoueurs[i], i-i%2, i%2);
        layout->addWidget(initJoueurs[i], i+1-i%2, i%2);
    }

    validation = new QPushButton("Valider l'initiative", this);
    validation->setMaximumSize(200, validation->height());
    layout->addWidget(validation, listeJoueurs.size(), 0);

    retour = new QPushButton("Retourner au menu", this);
    retour->setMaximumSize(150, retour->height());
    layout->addWidget(retour, listeJoueurs.size(), 1);

    for (int i = 0; i < listeJoueurs.size(); ++i) {
        QObject::connect(listeJoueurs[i], SIGNAL(clicked()), this, SLOT(disponibilite()));
    }
}

std::vector<std::shared_ptr<Personnage>> ChoixJoueurs::valider() {
    std::vector<std::shared_ptr<Personnage>> joueursPresents = std::vector<std::shared_ptr<Personnage>>();
    bool coherent = joueursExistants();

    for (int i = 0; i < listeJoueurs.size(); ++i) {
        if (listeJoueurs[i]->isChecked()) {
            if (initJoueurs[i]->text() != 0) {
                joueursPresents.push_back(joueurs[i]);
                joueurs[i]->setInitiative(initJoueurs[i]->text().toInt());
                listeJoueurs[i]->setStyleSheet("QCheckBox { color: black }");
                initJoueurs[i]->setStyleSheet("QLineEdit { background: white }");
            }
            else {
                coherent = false;
                listeJoueurs[i]->setStyleSheet("QCheckBox { color: red }");
                initJoueurs[i]->setStyleSheet("QLineEdit { background: red }");
            }
        }
        else {
            listeJoueurs[i]->setStyleSheet("QCheckBox { color: black }");
            initJoueurs[i]->setStyleSheet("QLineEdit { background: white }");
        }
    }

    if (coherent) {
        joueursPresents.insert(joueursPresents.end(), mobs.begin(), mobs.end());
        std::sort(joueursPresents.begin(), joueursPresents.end(), triNom);
        std::sort(joueursPresents.begin(), joueursPresents.end(), triInitiative);

        for (auto i = joueursPresents.begin(); i != joueursPresents.end(); ++i) {
            (*i)->activerBuffsDebutCombat();
        }
    }
    else {
        joueursPresents.clear();
    }

    return joueursPresents;
}

void ChoixJoueurs::disponibilite() {
    for (int i = 0; i < listeJoueurs.size(); ++i) {
        initJoueurs[i]->setEnabled(listeJoueurs[i]->isChecked());
    }
}

bool ChoixJoueurs::joueursExistants() {
    bool coherent = true;
    int joueurs = 0;

    for (auto i = listeJoueurs.begin(); i != listeJoueurs.end(); ++i) {
        if ((*i)->isChecked()) {
            joueurs++;
        }
    }

    if (!joueurs) {
        for (int i = 0; i < listeJoueurs.size(); ++i) {
            listeJoueurs[i]->setStyleSheet("QCheckBox { color: red }");
            initJoueurs[i]->setStyleSheet("QLineEdit { background: red }");
        }

        coherent = false;
    }

    return coherent;
}

QPushButton *ChoixJoueurs::getValidation() {
    return validation;
}

QPushButton *ChoixJoueurs::getRetour() {
    return retour;
}
