#include "../../../ressources/fenetres/interface/ChoixCombat.h"
#include <QFile>
#include <QTextStream>

std::vector<std::shared_ptr<Personnage>> creerCombat(QString chemin) {
    /* Ouverture fichier et flux de données */
    QFile fichier(chemin);
    fichier.open(QIODevice::ReadOnly);
    QTextStream donnees(&fichier);

    /* Variable de retour */
    std::vector<std::shared_ptr<Personnage>> mob = std::vector<std::shared_ptr<Personnage>>();

    /* Variables conteneurs */
    QString cheminMob;
    int nbMob;

    while (!donnees.atEnd()) {
        donnees >> nbMob;
        donnees >> cheminMob;

        for (int i = 0; i < nbMob; ++i) {
            mob.push_back(creerMob(cheminMob));
            mob.back()->setNom(mob.back()->getNom() + " " + QString::number(i + 1));
        }
    }
    return mob;
}

std::shared_ptr<Personnage> creerMob(QString chemin) {
    /* Ouverture fichier et flux de données */
    QFile fichier(chemin);
    fichier.open(QIODevice::ReadOnly);
    QTextStream donnees(&fichier);

    /* Variable de retour */
    std::shared_ptr<Personnage> mob = std::shared_ptr<Personnage>();

    /* Variable conteneurs */
    std::vector<std::shared_ptr<Buff>> buffs = std::vector<std::shared_ptr<Buff>>();
    std::vector<std::shared_ptr<Attaque>> attaques = std::vector<std::shared_ptr<Attaque>>();
    QString cheminDonnee, nom;
    int nbDonnee, id, CA, niveau, vieMax, vie, experience, init, reflexes, vigueur, volonte, resistance, camp;


    donnees >> nbDonnee; /* Nombre de buffs dans le mob */

    for (int i = 0; i < nbDonnee; ++i) {
        donnees >> cheminDonnee; /* Chemin du fichier du buff */
        buffs.push_back(creerBuff(cheminDonnee));
    }

    donnees >> nbDonnee; /* Nombre d'attaque du mob */

    for (int i = 0; i < nbDonnee; ++i) {
        donnees >> cheminDonnee; /* Chemin du fichier de l'attaque */
        attaques.push_back(creerAttaque(cheminDonnee));
    }

    /* Lecture des informations du mob */
    donnees >> id;
    donnees >> nom;
    donnees >> CA;
    donnees >> niveau;
    donnees >> vieMax;
    donnees >> vie;
    donnees >> init;
    donnees >> reflexes;
    donnees >> vigueur;
    donnees >> volonte;
    donnees >> resistance;
    donnees >> experience;
    donnees >> camp;

    mob = std::make_shared<Personnage>(id, nom.replace("_", " "), CA, niveau, vieMax, vie, attaques, experience, init, reflexes,
                                       vigueur, volonte, resistance, camp, buffs);

    return mob;
}


std::shared_ptr<Attaque> creerAttaque(QString chemin) {
    /* Ouverture fichier et flux de données */
    QFile fichier(chemin);
    fichier.open(QIODevice::ReadOnly);
    QTextStream donnees(&fichier);

    /* Variable de retour */
    std::shared_ptr<Attaque> attaque = std::shared_ptr<Attaque>();

    /* Variables conteneurs */
    std::vector<std::shared_ptr<Buff>> buffs = std::vector<std::shared_ptr<Buff>>();
    std::vector<std::shared_ptr<JetSauvegarde>> jetSauv = std::vector<std::shared_ptr<JetSauvegarde>>();
    std::vector<float> deDegats = std::vector<float>();
    QString cheminDonnee, nom;
    int nbDonnee, id, degats, toucher, critique, degatsCritiques, cooldown, toursRestants, detail;
    float deDegat;
    bool disponible;

    donnees >> nbDonnee; /* Nombre de buffs dans l'attaque */

    for (int i = 0; i < nbDonnee; ++i) {
        donnees >> cheminDonnee; /* Chemin du fichier du buff */
        buffs.push_back(creerBuff(cheminDonnee));
    }

    donnees >> nbDonnee; /* Nombre de jets de sauvegarde dans l'attaque */

    for (int i = 0; i < nbDonnee; ++i) {
        donnees >> cheminDonnee; /* Chemin du fichier du jet de sauvegarde */
        jetSauv.push_back(creerJetSauv(cheminDonnee));
    }

    donnees >> id;
    donnees >> nom;
    donnees >> deDegat;
    deDegats.push_back(deDegat);
    donnees >> degats;
    donnees >> toucher;
    donnees >> critique;
    donnees >> degatsCritiques;
    donnees >> cooldown;
    donnees >> toursRestants;
    donnees >> detail;
    disponible = toursRestants == 0;

    attaque = std::make_shared<Attaque>(id, nom.replace("_", " "), deDegats, degats, toucher, critique, degatsCritiques,
                                        cooldown, toursRestants, disponible, detail, buffs, jetSauv);

    return attaque;
}

std::shared_ptr<Buff> creerBuff(QString chemin) {
    /* Ouverture fichier et flux de données */
    QFile fichier(chemin);
    fichier.open(QIODevice::ReadOnly);
    QTextStream donnees(&fichier);

    /* Variable de retour */
    std::shared_ptr<Buff> buffs = std::shared_ptr<Buff>();

    /* Variables conteneurs */
    std::vector<std::shared_ptr<JetSauvegarde>> jetSauv = std::vector<std::shared_ptr<JetSauvegarde>>();
    std::vector<int> caracteristiques;
    std::vector<float> modificateurs;
    QString cheminDonnee, nom;
    int nbDonnee, duree, booleen, caracteristique;
    float modificateur;
    bool direct;

    donnees >> nbDonnee; /* Nombre de jets de sauvegarde */

    for (int i = 0; i < nbDonnee; ++i) {
        donnees >> cheminDonnee; /* Chemin du fichier du jet de sauvegarde */
        jetSauv.push_back(creerJetSauv(cheminDonnee));
    }

    donnees >> nom;
    donnees >> duree;
    donnees >> booleen;
    direct = booleen == 1;

    donnees >> nbDonnee; /* Nombre de caractéristiques modifiées */

    for (int i = 0; i < nbDonnee; ++i) {
        donnees >> caracteristique; /* Caractéristique modifiée */
        caracteristiques.push_back(caracteristique);

        donnees >> modificateur; /* Valeur de la modification */
        modificateurs.push_back(modificateur);
    }

    buffs = std::make_shared<Buff>(nom.replace("_", " "), duree, direct, caracteristiques, modificateurs, jetSauv);

    return buffs;
}

std::shared_ptr<JetSauvegarde> creerJetSauv(QString chemin) {
    /* Ouverture fichier et flux de données */
    QFile fichier(chemin);
    fichier.open(QIODevice::ReadOnly);
    QTextStream donnees(&fichier);

    /* Variable de retour */
    std::shared_ptr<JetSauvegarde> jetSauv = std::shared_ptr<JetSauvegarde>();

    int type, action;

    donnees >> type;
    donnees >> action;

    jetSauv = std::make_shared<JetSauvegarde>(type, action);

    return jetSauv;
}

std::vector<std::shared_ptr<Personnage>> creerJoueurs(QString chemin) {
    /* Ouverture fichier et flux de données */
    QFile fichier(chemin);
    fichier.open(QIODevice::ReadOnly);
    QTextStream donnees(&fichier);

    /* Variable de retour */
    std::vector<std::shared_ptr<Personnage>> joueurs = std::vector<std::shared_ptr<Personnage>>();

    int nbJoueurs, id, CA;
    QString nom;

    donnees >> nbJoueurs;

    for (int i = 0; i < nbJoueurs; ++i) {
        donnees >> id;
        donnees >> nom;
        donnees >> CA;

        joueurs.push_back(std::make_shared<Personnage>(id, nom.replace("_", " "), CA));
    }

    return joueurs;
}
