#include "ressources/fenetres/interface/Menu.h"
#include "ressources/jdr/Personnage.h"
#include "ressources/fenetres/interface/ChoixCombat.h"

#include <QFileDialog>
#include <QLayout>
#include <QGroupBox>

Menu::Menu(QWidget *parent) : QWidget(parent) {
    QGridLayout *layout = new QGridLayout();
    QGroupBox * groupe = new QGroupBox(this);

    combat = new QPushButton("Choisir combat", groupe);
    combat->setFixedSize(combat->size());
    layout->addWidget(combat, 0, 1);

    editeur = new QPushButton("Mode éditeur", groupe);
    editeur->setFixedSize(editeur->size());
    layout->addWidget(editeur, 1, 1);

    quitter = new QPushButton("Quitter", groupe);
    quitter->setFixedSize(quitter->size());
    layout->addWidget(quitter, 2, 1);

    groupe->setLayout(layout);

    QObject::connect(quitter, SIGNAL(clicked()), parentWidget(), SLOT(close()));
}

ChoixJoueurs *Menu::lancement() {
    std::vector<std::shared_ptr<Personnage>> joueurs, mobs;
    QString adrMob = QFileDialog::getOpenFileName(0, "Quel combat ouvrir ?", "/home/jozereau/Combat_JDR/Combats/Combats/");
    ChoixJoueurs *fenetre = 0;

    if (adrMob != 0) {
        mobs = creerCombat(adrMob);
        joueurs = creerJoueurs("/home/jozereau/Combat_JDR/Combats/Joueurs/Joueurs");

        fenetre = new ChoixJoueurs(joueurs, mobs, parentWidget());
    }

    return fenetre;
}

QPushButton *Menu::getEditeur() {
    return editeur;
}

QPushButton *Menu::getCombat() {
    return combat;
}
