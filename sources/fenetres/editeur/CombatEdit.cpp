#include "../../../ressources/fenetres/editeur/CombatEdit.h"

#include <QLayout>
#include <QFileDialog>
#include <QTextStream>

CombatEdit::CombatEdit(QWidget *parent) : QWidget(parent) {
    QGridLayout *layout = new QGridLayout();

    ouvrirCombat = new QPushButton("Ouvrir un combat existant", this);
    layout->addWidget(ouvrirCombat, 0, 2);

    ajoutMob = new QPushButton("Ajouter mob", this);
    indicMob = new QLabel("Nombre de mob", this);
    nbMob = new QSpinBox(this);
    nbMob->setMaximum(50);
    nbMob->setMinimum(1);
    layout->addWidget(indicMob, 1, 0);
    layout->addWidget(nbMob, 2, 0);
    layout->addWidget(ajoutMob, 2, 1);
    infoMob = new QTextBrowser(this);
    infoMob->setFixedSize(infoMob->maximumSize());
    infoMob->hide();
    infoMobLues = new QTextBrowser(this);
    layout->addWidget(infoMobLues, 3, 0, 1, 2);

    save = new QPushButton("Sauvegarder le combat", this);
    layout->addWidget(save, 5, 2);

    supprimerMob = new QPushButton("Supprimer les mobs");
    layout->addWidget(supprimerMob, 4, 0);

    this->setLayout(layout);

    QObject::connect(ouvrirCombat, SIGNAL(clicked()), this, SLOT(ouvertureCombat()));
    QObject::connect(ajoutMob, SIGNAL(clicked()), this, SLOT(choixMob()));
    QObject::connect(save, SIGNAL(clicked()), this, SLOT(creerCombat()));
    QObject::connect(supprimerMob, SIGNAL(clicked()), infoMob, SLOT(clear()));

}

void CombatEdit::creerCombat() {
    if (combatPret()) {
        QString chemin = QFileDialog::getSaveFileName(0, "Fichier de sauvegarde", "/home/jozereau/Combat_JDR/Combats/Combats/");

        if (chemin != 0) {
            QFile fichier(chemin);
            fichier.open(QIODevice::WriteOnly | QIODevice::Text);

            QTextStream donnees(&fichier);

            donnees << infoMob->toPlainText();

            fichier.close();

            reinitialiserCombat();
        }
    }
}

void CombatEdit::choixMob() {
    QString chemin = QFileDialog::getOpenFileName(0, "Choix du mob", "/home/jozereau/Combat_JDR/Combats/Mobs");

    if (chemin != 0) {
        infoMob->append(QString::number(nbMob->value()) + " " + chemin);
        infoMobLues->append(QString::number(nbMob->value()) + " " + chemin.remove("/home/jozereau/Combat_JDR/Combats/Mobs/"));
    }
}

void CombatEdit::ouvertureCombat() {
    QString chemin = QFileDialog::getOpenFileName(0, "Choix du combat", "/home/jozereau/Combat_JDR/Combats/Combats/");

    if (chemin != 0) {
        QFile fichier(chemin);
        fichier.open(QIODevice::ReadOnly);

        QTextStream donnees(&fichier);

        reinitialiserCombat();

        QString mobs = donnees.readAll();

        infoMob->append(mobs);
        infoMobLues->append(mobs.remove("/home/jozereau/Combat_JDR/Combats/Mobs/"));

        fichier.close();
    }
}

bool CombatEdit::combatPret() {
    bool pret = true;

    if (infoMob->toPlainText() == 0) {
        infoMobLues->setStyleSheet("QTextBrowser { background: red }");
        pret = false;
    }
    else infoMobLues->setStyleSheet("QTextBrowser { background: white }");

    return pret;
}

void CombatEdit::reinitialiserCombat() {
    infoMob->clear();
    infoMobLues->clear();
    nbMob->setValue(1);
}
