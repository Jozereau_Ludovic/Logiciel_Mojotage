#include "../../../ressources/fenetres/editeur/MobEdit.h"
#include "../../../ressources/utils.h"

#include <QFileDialog>
#include <QLayout>
#include <QTextStream>

MobEdit::MobEdit(QWidget *parent) : QWidget(parent) {
    QGridLayout *layout = new QGridLayout();
    nbBuff = 0;
    nbAttaque = 0;

    ouvrirMob = new QPushButton("Ouvrir un mob existant", this);
    layout->addWidget(ouvrirMob, 0, 3);

    ajoutAttaque = new QPushButton("Ajouter attaque", this);
    infoAttaque = new QTextBrowser(this);
    infoAttaque->setFixedSize(infoAttaque->maximumSize());
    infoAttaque->hide();
    infoAttaqueLues = new QTextBrowser(this);
    layout->addWidget(ajoutAttaque, 9, 0);
    layout->addWidget(infoAttaqueLues, 10, 0, 1, 2);

    supprimerAttaque = new QPushButton("Supprimer la dernière attaque", this);
    layout->addWidget(supprimerAttaque, 9, 1);

    ajoutBuff = new QPushButton("Ajouter buff", this);
    infoBuff = new QTextBrowser(this);
    infoBuff->setFixedSize(infoBuff->maximumSize());
    infoBuff->hide();
    infoBuffLues = new QTextBrowser(this);
    layout->addWidget(ajoutBuff, 11, 0);
    layout->addWidget(infoBuffLues, 12, 0, 1, 2);

    supprimerBuff = new QPushButton("Supprimer le dernier buff", this);
    layout->addWidget(supprimerBuff, 11, 1);

    save = new QPushButton("Sauvegarder mob", this);
    layout->addWidget(save, 12, 3);

    nom = new QLineEdit(this);
    infoNom = new QLabel("Nom mob", this);
    layout->addWidget(infoNom, 1, 0);
    layout->addWidget(nom, 2, 0);

    for (int i = 0; i < 9; ++i) {
        QSpinBox *carac = new QSpinBox(this);
        carac->setMinimum(0);
        carac->setMaximum(200);

        QLabel *infoCarac = new QLabel(this);

        caracteristiques.push_back(carac);
        infoCaracteristiques.push_back(infoCarac);
    }
    infoCaracteristiques[0]->setText("CA");
    layout->addWidget(infoCaracteristiques[0], 5, 1);
    layout->addWidget(caracteristiques[0], 6, 1);
    caracteristiques[0]->setMinimum(10);
    infoCaracteristiques[1]->setText("Niveau");
    layout->addWidget(infoCaracteristiques[1], 3, 0);
    layout->addWidget(caracteristiques[1], 4, 0);
    caracteristiques[1]->setMinimum(1);
    infoCaracteristiques[2]->setText("Vie Max");
    layout->addWidget(infoCaracteristiques[2], 3, 1);
    layout->addWidget(caracteristiques[2], 4, 1);
    caracteristiques[2]->setMaximum(10000);
    infoCaracteristiques[3]->setText("Vie");
    layout->addWidget(infoCaracteristiques[3], 3, 2);
    layout->addWidget(caracteristiques[3], 4, 2);
    infoCaracteristiques[4]->setText("Initiative");
    layout->addWidget(infoCaracteristiques[4], 5, 0);
    layout->addWidget(caracteristiques[4], 6, 0);
    infoCaracteristiques[5]->setText("Réflexes");
    layout->addWidget(infoCaracteristiques[5], 7, 0);
    layout->addWidget(caracteristiques[5], 8, 0);
    infoCaracteristiques[6]->setText("Vigueur");
    layout->addWidget(infoCaracteristiques[6], 7, 1);
    layout->addWidget(caracteristiques[6], 8, 1);
    infoCaracteristiques[7]->setText("Volonte");
    layout->addWidget(infoCaracteristiques[7], 7, 2);
    layout->addWidget(caracteristiques[7], 8, 2);
    infoCaracteristiques[8]->setText("Résistance");
    layout->addWidget(infoCaracteristiques[8], 5, 2);
    layout->addWidget(caracteristiques[8], 6, 2);

    experience = new QLineEdit("0", this);
    infoExperience = new QLabel("Xp donné", this);
    layout->addWidget(infoExperience, 1, 2);
    layout->addWidget(experience, 2, 2);

    camp = new QComboBox(this);
    camp->addItem("Ennemi");
    camp->addItem("Allié");
    infoCamp = new QLabel("Camp", this);
    layout->addWidget(infoCamp, 1, 1);
    layout->addWidget(camp, 2, 1);

    this->setLayout(layout);

    QObject::connect(caracteristiques[2], SIGNAL(valueChanged(int)), this, SLOT(coherenceVie(int)));
    QObject::connect(save, SIGNAL(clicked()), this, SLOT(creerMob()));
    QObject::connect(ajoutAttaque, SIGNAL(clicked()), this, SLOT(choixAttaque()));
    QObject::connect(ajoutBuff, SIGNAL(clicked()), this, SLOT(choixBuff()));
    QObject::connect(ouvrirMob, SIGNAL(clicked()), this, SLOT(ouvertureMob()));
    QObject::connect(supprimerBuff, SIGNAL(clicked()), this, SLOT(annulerBuff()));
    QObject::connect(supprimerAttaque, SIGNAL(clicked()), this, SLOT(annulerAttaque()));
}

void MobEdit::coherenceVie(int vieMax) {
    caracteristiques[3]->setMaximum(vieMax);
    caracteristiques[3]->setValue(caracteristiques[3]->maximum());
}

void MobEdit::creerMob() {
    if (mobPret()) {
        QString chemin = QFileDialog::getSaveFileName(0, "Fichier de sauvegarde",
                                                      "/home/jozereau/Combat_JDR/Combats/Mobs/");

        if (chemin != 0) {
            QFile fichier(chemin);
            fichier.open(QIODevice::WriteOnly | QIODevice::Text);

            QTextStream donnees(&fichier);

            donnees << QString::number(nbBuff) + "\n";
            donnees << infoBuff->toPlainText() + "\n";

            donnees << QString::number(nbAttaque) + "\n";
            donnees << infoAttaque->toPlainText() + "\n";

            donnees << QString::number(compterFichier("/home/jozereau/Combat_JDR/Combats/Mobs/") + 6) + " " + nom->text().replace(" ", "_");
            for (int i = 0; i < 9; ++i) {
                donnees << " " + caracteristiques[i]->text();
            }

            donnees << " " + experience->text() + " " + QString::number(camp->currentIndex());
            fichier.close();

            reinitialiserMob();
        }
    }
}

void MobEdit::choixAttaque() {
    QString chemin = QFileDialog::getOpenFileName(0, "Choix de l'attaque",
                                                  "/home/jozereau/Combat_JDR/Combats/Attaques/");

    if (chemin != 0) {
        nbAttaque++;
        infoAttaque->append(chemin);
        infoAttaqueLues->append(chemin.remove("/home/jozereau/Combat_JDR/Combats/Attaques/"));
    }
}

void MobEdit::choixBuff() {
    QString chemin = QFileDialog::getOpenFileName(0, "Choix du buff",
                                                  "/home/jozereau/Combat_JDR/Combats/Buffs/NonAttaque");

    if (chemin != 0) {
        nbBuff++;
        infoBuff->append(chemin);
        infoBuffLues->append(chemin.remove("/home/jozereau/Combat_JDR/Combats/Buffs/"));
    }

}

bool MobEdit::mobPret() {
    bool pret = true;

    if (nom->text() == 0) {
        nom->setStyleSheet("QLineEdit { background: red }");
        pret = false;
    }
    else nom->setStyleSheet("QLineEdit { background: white }");

    if (nbAttaque == 0) {
        ajoutAttaque->setStyleSheet("QPushButton { color: red }");
        pret = false;
    }
    else ajoutAttaque->setStyleSheet("QPushButton { color: black }");

    return pret;
}

void MobEdit::ouvertureMob() {
    QString chemin = QFileDialog::getOpenFileName(0, "Choix de l'attaque", "/home/jozereau/Combat_JDR/Combats/Mobs/");

    if (chemin != 0) {
        QFile fichier(chemin);
        fichier.open(QIODevice::ReadOnly);

        QTextStream donnees(&fichier);
        QString adresses;
        int carac;

        reinitialiserMob();

        donnees >> nbBuff;
        for (int i = 0; i < nbBuff; ++i) {
            donnees >> adresses;
            infoBuff->append(adresses);
            infoBuffLues->append(adresses.remove("/home/jozereau/Combat_JDR/Combats/Buffs/"));
        }

        donnees >> nbAttaque;
        for (int i = 0; i < nbAttaque; ++i) {
            donnees >> adresses;
            infoAttaque->append(adresses);
            infoAttaqueLues->append(adresses.remove("/home/jozereau/Combat_JDR/Combats/Attaques/"));
        }

        donnees >> carac;
        donnees >> adresses;
        nom->setText(adresses.replace("_", " "));

        for (int i = 0; i < 9; ++i) {
            donnees >> carac;
            caracteristiques[i]->setValue(carac);
        }

        donnees >> carac;
        experience->setText(QString::number(carac));

        donnees >> carac;
        camp->setCurrentIndex(carac);

        fichier.close();
    }

}

void MobEdit::reinitialiserMob() {
    nom->clear();
    experience->setText("0");
    infoAttaque->clear();
    infoAttaqueLues->clear();
    infoBuff->clear();
    infoBuffLues->clear();
    nbAttaque = 0;
    nbBuff = 0;
    camp->setCurrentIndex(0);

    for (int i = 0; i < 9; ++i) {
        caracteristiques[i]->setValue(0);
    }
}

void MobEdit::annulerBuff() {
    if (nbBuff != 0) {
        effacerLigne(infoBuff, nbBuff--);
        effacerLigne(infoBuffLues, nbBuff);
    }
}

void MobEdit::annulerAttaque() {
    if (nbAttaque != 0) {
        effacerLigne(infoAttaque, nbAttaque--);
        effacerLigne(infoAttaqueLues, nbAttaque);
    }
}
