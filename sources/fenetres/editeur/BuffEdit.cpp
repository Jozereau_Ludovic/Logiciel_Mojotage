#include "../../../ressources/fenetres/editeur/BuffEdit.h"
#include "../../../ressources/utils.h"
#include "../../../ressources/jdr/De.h"

#include <QLayout>
#include <QFileDialog>
#include <QTextStream>

BuffEdit::BuffEdit(QWidget *parent) : QWidget(parent) {
    QGridLayout *layout = new QGridLayout();

    ouvrirBuff = new QPushButton("Ouvrir un buff existant", this);
    layout->addWidget(ouvrirBuff, 0, 3);

    save = new QPushButton("Sauvegarder buff", this);
    layout->addWidget(save, 11, 3);

    nom = new QLineEdit(this);
    infoNom = new QLabel("Nom du buff", this);
    layout->addWidget(infoNom, 1, 0);
    layout->addWidget(nom, 2, 0);

    ajoutModif = new QPushButton("Ajouter un effet", this);
    layout->addWidget(ajoutModif, 6, 0);
    caracteristique = new QComboBox(this);
    caracteristique->addItem("Vie max");
    caracteristique->addItem("Vie");
    caracteristique->addItem("CA");
    caracteristique->addItem("Réflexes");
    caracteristique->addItem("Vigueur");
    caracteristique->addItem("Volonté");
    caracteristique->addItem("Résistance");
    caracteristique->addItem("Dégâts");
    caracteristique->addItem("Dés de dégâts");
    caracteristique->addItem("Toucher");
    caracteristique->addItem("Critique");
    caracteristique->addItem("Dégâts critiques");
    caracteristique->addItem("Débuff");
    modificateur = new QLineEdit(this);
    modificateurDesType = new QLineEdit(this);
    modificateurDesFaces = new QLineEdit(this);
    infoCarac = new QLabel("Choix de l'effet");
    infoModif = new QTextBrowser(this);
    infoModif->setFixedSize(infoModif->maximumSize());
    infoModif->hide();
    infoModifLues = new QTextBrowser(this);
    layout->addWidget(infoCarac, 3, 0);
    layout->addWidget(caracteristique, 4, 0);
    layout->addWidget(modificateur, 4, 1);
    layout->addWidget(modificateurDesType, 4, 1);
    layout->addWidget(modificateurDesFaces, 5, 1);
    modificateurDesType->hide();
    modificateurDesFaces->hide();
    layout->addWidget(infoModifLues, 7, 0, 1, 2);
    nbModif = 0;

    supprimerModif = new QPushButton("Supprimer le dernier effet", this);
    layout->addWidget(supprimerModif, 6, 1);

    ajoutJetSauv = new QPushButton("Ajouter un jet de sauvegarde", this);
    layout->addWidget(ajoutJetSauv, 10, 0);
    infoJetSauv = new QTextBrowser(this);
    infoJetSauv->setFixedSize(infoJetSauv->maximumSize());
    infoJetSauv->hide();
    infoJetSauvLues = new QTextBrowser(this);
    layout->addWidget(infoJetSauvLues, 11, 0, 1, 2);

    supprimerJetSauv = new QPushButton("Supprimer le dernier jet de sauvegarde", this);
    layout->addWidget(supprimerJetSauv, 10, 1);

    duree = new QSpinBox(this);
    duree->setMinimum(-1);
    duree->setMaximum(50);
    infoDuree = new QLabel("Durée de l'effet (-1 pour infini)", this);
    layout->addWidget(infoDuree, 1, 1);
    layout->addWidget(duree, 2, 1);

    direct = new QComboBox(this);
    direct->addItem("Chaque tours");
    direct->addItem("Une seule fois");
    infoDirect = new QLabel("Mode d'activation du buff", this);
    layout->addWidget(infoDirect, 8, 0);
    layout->addWidget(direct, 9, 0);

    this->setLayout(layout);

    QObject::connect(save, SIGNAL(clicked()), this, SLOT(creerBuff()));
    QObject::connect(ouvrirBuff, SIGNAL(clicked()), this, SLOT(ouvertureBuff()));
    QObject::connect(ajoutModif, SIGNAL(clicked()), this, SLOT(choixModif()));
    QObject::connect(ajoutJetSauv, SIGNAL(clicked()), this, SLOT(choixJetSauv()));
    QObject::connect(supprimerModif, SIGNAL(clicked()), this, SLOT(annulerModif()));
    QObject::connect(supprimerJetSauv, SIGNAL(clicked()), this, SLOT(annulerJetSauv()));
    QObject::connect(caracteristique, SIGNAL(currentIndexChanged(int)), this, SLOT(adapterCarac(int)));
}

void BuffEdit::creerBuff() {
    if (buffPret()) {
        QString chemin;
        chemin = QFileDialog::getSaveFileName(0, "Fichier de sauvegarde",
                                              "/home/jozereau/Combat_JDR/Combats/Buffs/");

        if (chemin != 0) {
            QFile fichier(chemin);
            fichier.open(QIODevice::WriteOnly | QIODevice::Text);

            QTextStream donnees(&fichier);

            donnees << QString::number(nbJetSauv) + "\n";
            donnees << infoJetSauv->toPlainText() + "\n";

            donnees << nom->text().replace(" ", "_") + " " + QString::number(duree->value()) + " " + QString::number(direct->currentIndex()) + "\n";
            donnees << QString::number(nbModif) + "\n" + infoModif->toPlainText();

            fichier.close();

            reinitialiserBuff();
        }
    }
}

void BuffEdit::choixModif() {
    if (caracteristique->currentIndex() == 8) {
        if (modificateurDesType->text() != 0 || modificateurDesFaces->text() != 0) {
            nbModif++;
            infoModif->append(QString::number(caracteristique->currentIndex()) + " " + QString::number(convDeDegats(modificateurDesType->text().toInt(),
                                                                                                                    modificateurDesFaces->text().toInt())));
            infoModifLues->append(caracBuff(caracteristique->currentIndex()) + " " + modificateurDesType->text() + "d" + modificateurDesFaces->text());

            modificateurDesType->setStyleSheet("QLineEdit { background: white }");
            modificateurDesFaces->setStyleSheet("QLineEdit { background: white }");
            caracteristique->setCurrentIndex(0);
            modificateurDesType->clear();
            modificateurDesFaces->clear();
        }
        else {
            modificateurDesType->setStyleSheet("QLineEdit { background: red }");
            modificateurDesFaces->setStyleSheet("QLineEdit { background: red }");
        }
    }

    if (modificateur->text() != 0) {
        nbModif++;
        infoModif->append(QString::number(caracteristique->currentIndex()) + " " + modificateur->text());
        infoModifLues->append(caracBuff(caracteristique->currentIndex()) + " " + modificateur->text());

        modificateur->setStyleSheet("QLineEdit { background: white }");
        caracteristique->setCurrentIndex(0);
        modificateur->clear();
    }
    else modificateur->setStyleSheet("QLineEdit { background: red }");
}

void BuffEdit::choixJetSauv() {
    QString chemin = QFileDialog::getOpenFileName(0, "Choix du buff",
                                                  "/home/jozereau/Combat_JDR/Combats/JetSauv/");

    if (chemin != 0) {
        nbJetSauv++;
        infoJetSauv->append(chemin);
        infoJetSauvLues->append(chemin.remove("/home/jozereau/Combat_JDR/Combats/JetSauv/"));
    }
}

void BuffEdit::ouvertureBuff() {
    QString chemin = QFileDialog::getOpenFileName(0, "Choix de l'attaque", "/home/jozereau/Combat_JDR/Combats/Buffs/");

    if (chemin != 0) {
        QFile fichier(chemin);
        fichier.open(QIODevice::ReadOnly);

        QTextStream donnees(&fichier);
        QString adresses;
        float carac, modif;

        reinitialiserBuff();

        donnees >> nbJetSauv;
        for (int i = 0; i < nbJetSauv; ++i) {
            donnees >> adresses;
            infoJetSauv->append(adresses);
            infoJetSauvLues->append(adresses.remove("/home/jozereau/Combat_JDR/Combats/JetSauv/"));
        }

        donnees >> adresses;
        nom->setText(adresses.replace("_", " "));

        donnees >> carac;
        duree->setValue(carac);
        donnees >> carac;
        direct->setCurrentIndex(carac);

        donnees >> nbModif;
        for (int i = 0; i < nbModif; ++i) {
            donnees >> carac;
            donnees >> modif;
            infoModif->append(QString::number(carac) + " " + QString::number(modif));
            infoModifLues->append(caracBuff(carac) + " " + QString::number(modif));
        }

        fichier.close();
    }
}

bool BuffEdit::buffPret() {
    bool pret = true;

    if (nom->text() == 0) {
        nom->setStyleSheet("QLineEdit { background: red }");
        pret = false;
    }
    else nom->setStyleSheet("QLineEdit { background: white }");

    if (nbModif == 0) {
        ajoutModif->setStyleSheet("QPushButton { color: red }");
        pret = false;
    }
    else {
        ajoutModif->setStyleSheet("QPushButton { color: black }");
    }

    return pret;
}

void BuffEdit::reinitialiserBuff() {
    nom->clear();
    caracteristique->setCurrentIndex(0);
    modificateur->clear();
    infoModif->clear();
    infoModifLues->clear();
    infoJetSauv->clear();
    infoJetSauvLues->clear();
    nbModif = 0;
    nbJetSauv = 0;
    duree->setValue(0);
    direct->setCurrentIndex(0);

}

void BuffEdit::annulerModif() {
    if (nbModif != 0) {
        effacerLigne(infoModif, nbModif--);
        effacerLigne(infoModifLues, nbModif);
    }
}

void BuffEdit::annulerJetSauv() {
    if (nbJetSauv != 0) {
        effacerLigne(infoJetSauv, nbJetSauv--);
        effacerLigne(infoJetSauvLues, nbJetSauv);
    }
}


void BuffEdit::adapterCarac(int carac) {
    if (carac == (int) constantes::caracteristique::DEDEGATS) {
        modificateurDesType->show();
        modificateurDesFaces->show();
        modificateur->hide();
        infoCarac->setText("Indiquer le nombre et le type de dés à ajouter ou retirer");
    }
    else {
        modificateurDesType->hide();
        modificateurDesFaces->hide();
        modificateur->show();
        infoCarac->setText("Choix de l'effet");
    }
}
