#include <QFileDialog>
#include <QLayout>
#include <QTextStream>

#include "ressources/utils.h"
#include "ressources/fenetres/editeur/Editeur.h"
#include "ressources/fenetres/editeur/MobEdit.h"
#include "ressources/fenetres/editeur/AttaqueEdit.h"
#include "ressources/fenetres/editeur/BuffEdit.h"
#include "ressources/fenetres/editeur/CombatEdit.h"
#include "ressources/fenetres/editeur/JetSauvEdit.h"

Editeur::Editeur(QWidget *parent) : QWidget(parent) {
    editeur = new QTabWidget(this);

    CombatEdit *combat = new CombatEdit();
    MobEdit *mob = new MobEdit();
    AttaqueEdit *attaque = new AttaqueEdit();
    BuffEdit *buff = new BuffEdit();
    JetSauvEdit *jetSauv = new JetSauvEdit();


    editeur->addTab(combat, "Combat");
    editeur->addTab(mob, "Mob");
    editeur->addTab(attaque, "Attaque");
    editeur->addTab(buff, "Buff");
    editeur->addTab(jetSauv, "Jet de sauvegarde");

    QGridLayout *layout = new QGridLayout();
    layout->addWidget(editeur);

    retour = new QPushButton("Retourner au menu");
    layout->addWidget(retour);

    setLayout(layout);
}

QPushButton *Editeur::getRetour() {
    return retour;
}
