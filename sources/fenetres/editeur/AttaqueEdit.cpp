#include "../../../ressources/fenetres/editeur/AttaqueEdit.h"
#include "../../../ressources/utils.h"
#include "../../../ressources/jdr/De.h"

#include <QFileDialog>
#include <QLayout>
#include <QTextStream>

AttaqueEdit::AttaqueEdit(QWidget *parent) : QWidget(parent) {
    QGridLayout *layout = new QGridLayout();

    nbBuff = 0;
    nbJetSauv = 0;

    ouvrirAttaque = new QPushButton("Ouvrir une attaque existante", this);
    layout->addWidget(ouvrirAttaque, 0, 2);

    ajoutBuff = new QPushButton("Ajouter un buff", this);
    layout->addWidget(ajoutBuff, 12, 0);
    infoBuff = new QTextBrowser(this);
    infoBuff->setFixedSize(infoBuff->maximumSize());
    infoBuff->hide();
    infoBuffLues = new QTextBrowser(this);
    layout->addWidget(infoBuffLues, 13, 0, 1, 2);

    supprimerBuff = new QPushButton("Supprimer le dernier buff", this);
    layout->addWidget(supprimerBuff, 12, 1);

    ajoutJetSauv = new QPushButton("Ajouter un jet de sauvegarde", this);
    layout->addWidget(ajoutJetSauv, 14, 0);
    infoJetSauv = new QTextBrowser(this);
    infoJetSauv->setFixedSize(infoJetSauv->maximumSize());
    infoJetSauv->hide();
    infoJetSauvLues = new QTextBrowser(this);
    layout->addWidget(infoJetSauvLues, 15, 0, 1, 2);

    supprimerJetSauv = new QPushButton("Supprimer le dernier jet de sauvegarde", this);
    layout->addWidget(supprimerJetSauv, 14, 1);

    save = new QPushButton("Sauvegarder l'attaque", this);
    layout->addWidget(save, 15, 2);

    nom = new QLineEdit(this);
    infoNom = new QLabel("Nom de l'attaque", this);
    layout->addWidget(infoNom, 1, 0);
    layout->addWidget(nom, 2, 0);

    nbDe = new QLineEdit("0", this);
    infoNbDe = new QLabel("Nombre de dé", this);
    layout->addWidget(infoNbDe, 3, 0);
    layout->addWidget(nbDe, 4, 0);

    nbFace = new QLineEdit("0", this);
    infoTypeDe = new QLabel("Type de dé", this);
    layout->addWidget(infoTypeDe, 3, 1);
    layout->addWidget(nbFace, 4, 1);

    for (int i = 0; i < 6; ++i) {
        QSpinBox *carac = new QSpinBox(this);
        carac->setMinimum(0);
        carac->setMaximum(200);

        QLabel *infoCarac = new QLabel(this);

        caracteristiques.push_back(carac);
        infoCaracteristiques.push_back(infoCarac);

        layout->addWidget(infoCarac, 5+i-(i%2), i%2);
        layout->addWidget(carac, 6+i-(i%2), i%2);
    }
    infoCaracteristiques[0]->setText("Bonus de dégâts");
    infoCaracteristiques[1]->setText("Bonus de toucher");
    infoCaracteristiques[2]->setText("Critique");
    caracteristiques[2]->setValue(20);
    caracteristiques[2]->setMaximum(20);
    infoCaracteristiques[3]->setText("Multiplicateur critique");
    caracteristiques[3]->setMinimum(2);
    infoCaracteristiques[4]->setText("Cooldown");
    infoCaracteristiques[5]->setText("Tours restants de base");

    detail = new QComboBox(this);
    detail->addItem("Aucun");
    detail->addItem("Sort");
    detail->addItem("Vol de vie");
    detail->addItem("Soins");
    detail->addItem("Buff");
    infoDetail = new QLabel("Détails de l'attaque", this);
    layout->addWidget(infoDetail, 1, 1);
    layout->addWidget(detail, 2, 1);

    this->setLayout(layout);

    QObject::connect(save, SIGNAL(clicked()), this, SLOT(creerAttaque()));
    QObject::connect(ajoutBuff, SIGNAL(clicked()), this, SLOT(choixBuff()));
    QObject::connect(ajoutJetSauv, SIGNAL(clicked()), this, SLOT(choixJetSauv()));
    QObject::connect(ouvrirAttaque, SIGNAL(clicked()), this, SLOT(ouvertureAttaque()));
    QObject::connect(supprimerBuff, SIGNAL(clicked()), this, SLOT(annulerBuff()));
    QObject::connect(supprimerJetSauv, SIGNAL(clicked()), this, SLOT(annulerJetSauv()));
    QObject::connect(detail, SIGNAL(currentIndexChanged(int)), this, SLOT(adapterDetail(int)));
}

void AttaqueEdit::creerAttaque() {
    if (attaquePret()) {
        QString chemin = QFileDialog::getSaveFileName(0, "Fichier de sauvegarde",
                                                      "/home/jozereau/Combat_JDR/Combats/Attaques/");

        if (chemin != 0) {
            QFile fichier(chemin);
            fichier.open(QIODevice::WriteOnly | QIODevice::Text);

            QTextStream donnees(&fichier);

            donnees << QString::number(nbBuff) + "\n";
            donnees << infoBuff->toPlainText() + "\n";

            donnees << QString::number(nbJetSauv) + "\n";
            donnees << infoJetSauv->toPlainText() + "\n";

            donnees << QString::number(compterFichier("/home/jozereau/Combat_JDR/Combats/Attaques/")) + " " + nom->text().replace(" ", "_")
                       + " " + QString::number(convDeDegats(nbDe->text().toInt(), nbFace->text().toInt()));
            for (int i = 0; i < 6; ++i) {
                donnees << " " + caracteristiques[i]->text();
            }

            donnees << " " + QString::number(detail->currentIndex());
            fichier.close();

            reinitialiserAttaque();
        }
    }
}

void AttaqueEdit::choixBuff() {
    QString chemin = QFileDialog::getOpenFileName(0, "Choix du buff",
                                                  "/home/jozereau/Combat_JDR/Combats/Buffs/");

    if (chemin != 0) {
        nbBuff++;
        infoBuff->append(chemin);
        infoBuffLues->append(chemin.remove("/home/jozereau/Combat_JDR/Combats/Buffs/"));
    }
}

void AttaqueEdit::choixJetSauv() {
    QString chemin = QFileDialog::getOpenFileName(0, "Choix du buff",
                                                  "/home/jozereau/Combat_JDR/Combats/JetSauv/");

    if (chemin != 0) {
        nbJetSauv++;
        infoJetSauv->append(chemin);
        infoJetSauvLues->append(chemin.remove("/home/jozereau/Combat_JDR/Combats/JetSauv/"));
    }
}

void AttaqueEdit::ouvertureAttaque() {
    QString chemin = QFileDialog::getOpenFileName(0, "Choix de l'attaque",
                                                  "/home/jozereau/Combat_JDR/Combats/Attaques/");

    if (chemin != 0) {
        reinitialiserAttaque();
        QFile fichier(chemin);
        fichier.open(QIODevice::ReadOnly);

        QTextStream donnees(&fichier);
        QString adresses;
        float carac;

        donnees >> nbBuff;
        for (int i = 0; i < nbBuff; ++i) {
            donnees >> adresses;
            infoBuff->append(adresses);
            infoBuffLues->append(adresses.remove("/home/jozereau/Combat_JDR/Combats/Buffs/"));
        }

        donnees >> nbJetSauv;
        for (int i = 0; i < nbJetSauv; ++i) {
            donnees >> adresses;
            infoJetSauv->append(adresses);
            infoJetSauvLues->append(adresses.remove("/home/jozereau/Combat_JDR/Combats/JetSauv/"));
        }

        donnees >> carac;
        donnees >> adresses;
        nom->setText(adresses.replace("_", " "));

        donnees >> carac;
        nbDe->setText(QString::number(std::floor(carac)));
        nbFace->setText(QString::number(typeDe(carac)));


        for (int i = 0; i < 6; ++i) {
            donnees >> carac;
            caracteristiques[i]->setValue(carac);
        }

        donnees >> carac;
        detail->setCurrentIndex(carac);

        fichier.close();
    }
}

bool AttaqueEdit::attaquePret() {
    bool pret = true;

    if (nom->text() == 0) {
        nom->setStyleSheet("QLineEdit { background: red }");
        pret = false;
    }
    else nom->setStyleSheet("QLineEdit { background: white }");

    if (detail->currentIndex() != 4) {
        if (nbDe->text() == 0) {
            nbDe->setStyleSheet("QLineEdit { background: red }");
            pret = false;
        }
        else nbDe->setStyleSheet("QLineEdit { background: white }");

        if (nbFace->text() == 0) {
            nbFace->setStyleSheet("QLineEdit { background: red }");
            pret = false;
        }
        else nbFace->setStyleSheet("QLineEdit { background: white }");
    }
    else {
        nbDe->setStyleSheet("QLineEdit { background: white }");
        nbFace->setStyleSheet("QLineEdit { background: white }");
    }

    return pret;
}

void AttaqueEdit::reinitialiserAttaque() {
    nom->clear();
    nbDe->setText("0");
    nbFace->setText("0");;
    infoBuff->clear();
    infoBuffLues->clear();
    infoJetSauv->clear();
    infoJetSauvLues->clear();
    nbBuff = 0;
    nbJetSauv = 0;
    detail->setCurrentIndex(0);

    for (int i = 0; i < 6; ++i) {
        caracteristiques[i]->setValue(0);
    }
}

void AttaqueEdit::annulerBuff() {
    if (nbBuff != 0) {
        effacerLigne(infoBuff, nbBuff--);
        effacerLigne(infoBuffLues, nbBuff);
    }
}

void AttaqueEdit::annulerJetSauv() {
    if (nbJetSauv != 0) {
        effacerLigne(infoJetSauv, nbJetSauv--);
        effacerLigne(infoJetSauvLues, nbJetSauv);
    }
}

void AttaqueEdit::adapterDetail(int index) {
    switch (index) {
    case (int) constantes::attaque::AUCUN:
    case (int) constantes::attaque::VOLVIE:
        for (int i = 0; i < 6; ++i) {
            caracteristiques[i]->setEnabled(true);
        }
        nbDe->setEnabled(true);
        nbFace->setEnabled(true);
        break;

    case (int) constantes::attaque::BUFF:
        for (int i = 0; i < 4; ++i) {
            caracteristiques[i]->setEnabled(false);
        }
        for (int i = 4; i < 6; ++i) {
            caracteristiques[i]->setEnabled(true);
        }
        nbDe->setEnabled(false);
        nbFace->setEnabled(false);
        break;

    case (int) constantes::attaque::SOINS:
    case (int) constantes::attaque::SORT:
        caracteristiques[0]->setEnabled(true);
        for (int i = 1; i < 4; ++i) {
            caracteristiques[i]->setEnabled(false);
        }
        for (int i = 4; i < 6; ++i) {
            caracteristiques[i]->setEnabled(true);
        }
        nbDe->setEnabled(true);
        nbFace->setEnabled(true);
        break;
    }
}
