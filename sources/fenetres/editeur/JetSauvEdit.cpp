#include "ressources/fenetres/editeur/JetSauvEdit.h"

#include <QLayout>
#include <QGroupBox>
#include <QTextStream>
#include <QFile>
#include <QFileDialog>

JetSauvEdit::JetSauvEdit(QWidget *parent) : QWidget(parent) {
    QGridLayout *layout = new QGridLayout();

    save = new QPushButton("Sauvegarder le jet", this);
    layout->addWidget(save, 3, 2);

    ouvrirJetSauv = new QPushButton("Ouvrir un jet de sauvegarde existant", this);
    layout->addWidget(ouvrirJetSauv, 0, 2);

    QGroupBox *groupeType = new QGroupBox(this);
    QGridLayout *layoutType = new QGridLayout();
    infoType = new QLabel("Type de jet", groupeType);
    type = new QComboBox(groupeType);
    type->addItem("Réflexes");
    type->addItem("Vigueur");
    type->addItem("Volonté");
    layoutType->addWidget(infoType, 0, 0);
    layoutType->addWidget(type, 0, 1);
    groupeType->setLayout(layoutType);
    layout->addWidget(groupeType, 1, 0);

    QGroupBox *groupeAction = new QGroupBox(this);
    QGridLayout *layoutAction = new QGridLayout();
    infoAction = new QLabel("Effet du jet", groupeAction);
    action = new QComboBox(groupeAction);
    action->addItem("Annule");
    action->addItem("Partiel");
    action->addItem("Moitié");
    action->addItem("Dévoile");
    layoutAction->addWidget(infoAction, 0, 0);
    layoutAction->addWidget(action, 0, 1);
    groupeAction->setLayout(layoutAction);
    layout->addWidget(groupeAction, 1, 1);

    this->setLayout(layout);

    QObject::connect(save, SIGNAL(clicked()), this, SLOT(creerJetSauv()));
    QObject::connect(ouvrirJetSauv, SIGNAL(clicked()), this, SLOT(ouvertureJetSauv()));
}

void JetSauvEdit::creerJetSauv() {
    QString chemin = QFileDialog::getSaveFileName(0, "Fichier de sauvegarde", "/home/jozereau/Combat_JDR/Combats/JetSauv/");

    if (chemin != 0) {
        QFile fichier(chemin);
        fichier.open(QIODevice::WriteOnly | QIODevice::Text);

        QTextStream donnees(&fichier);

        donnees << QString::number(type->currentIndex()) + " " + QString::number(action->currentIndex());

        reinitialiserJetSauv();
    }
}

void JetSauvEdit::ouvertureJetSauv() {
    QString chemin = QFileDialog::getOpenFileName(0, "Choix de l'attaque", "/home/jozereau/Combat_JDR/Combats/JetSauv/");

    if (chemin != 0) {
        QFile fichier(chemin);
        fichier.open(QIODevice::ReadOnly);

        QTextStream donnees(&fichier);

        int info;

        donnees >> info;
        type->setCurrentIndex(info);

        donnees >> info;
        action->setCurrentIndex(info);
    }
}

void JetSauvEdit::reinitialiserJetSauv() {
    type->setCurrentIndex(0);
    action->setCurrentIndex(0);
}
