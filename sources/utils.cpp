#include "ressources/utils.h"
#include <string.h>

#include <QFile>
#include <QTextStream>

int compterFichier(QString chemin) {
    int nbr = 0;
    struct dirent *ent = nullptr;
    DIR *dir = opendir(chemin.toLatin1());

    while ((ent = readdir(dir)) != NULL) {
        if (strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0) {
            nbr++;
        }
    }

    closedir(dir);

    return nbr;
}

QString caracBuff(int caracteristique) {
    QString carac = 0;

    switch (caracteristique) {
    case (int) constantes::caracteristique::CA:
        carac = "CA";
        break;
    case (int) constantes::caracteristique::CRITIQUE:
        carac = "Critique";
        break;
    case (int) constantes::caracteristique::DEDEGATS:
        carac = "Dés de dégâts";
        break;
    case (int) constantes::caracteristique::DEGATS:
        carac = "Bonus de dégâts";
        break;
    case (int) constantes::caracteristique::DEGCRIT:
        carac = "Dégâts critiques";
        break;
    case (int) constantes::caracteristique::REFLEXES:
        carac = "Réflexes";
        break;
    case (int) constantes::caracteristique::RESISTANCE:
        carac = "Résistance";
        break;
    case (int) constantes::caracteristique::TOUCHER:
        carac = "Toucher";
        break;
    case (int) constantes::caracteristique::VIE:
        carac = "Vie";
        break;
    case (int) constantes::caracteristique::VIEMAX:
        carac = "Vie max";
        break;
    case (int) constantes::caracteristique::VIGUEUR:
        carac = "Vigueur";
        break;
    case (int) constantes::caracteristique::VOLONTE:
        carac = "Volonte";
        break;
    case (int) constantes::caracteristique::DEBUFF:
        carac = "Débuff";
        break;
    }

    return carac;
}

QString detailAttaque(int detail) {
    QString retour = 0;

    switch (detail) {
    case (int) constantes::attaque::AUCUN:
    case (int) constantes::attaque::VOLVIE:
        retour = "Attaquer";
        break;
    case (int) constantes::attaque::BUFF:
        retour = "Lancer un buff";
        break;
    case (int) constantes::attaque::SOINS:
        retour = "Lancer un soin";
        break;
    case (int) constantes::attaque::SORT:
        retour = "Lancer un sort";
        break;
    }

    return retour;
}

void afficher(QString donnees) {
    QFile fichier("/home/jozereau/Combat_JDR/Combats/Sortie");
    fichier.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append);
    QTextStream ajout(&fichier);

    ajout << donnees + "\n";
    fichier.close();

}

void afficher(QTextBrowser *texte) {
    QFile fichier("/home/jozereau/Combat_JDR/Combats/Sortie");
    fichier.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream donnees(&fichier);

    texte->setText(donnees.readAll());
    QTextCursor curseur = texte->textCursor();
    curseur.movePosition(QTextCursor::End);
    texte->setTextCursor(curseur);
}

void effacerLigne(QTextBrowser * tableau, int ligne) {
    QTextCursor curseur = tableau->textCursor();
    curseur.movePosition(QTextCursor::Start);

    for(int i = 1; i < ligne; i++) {
        curseur.movePosition(QTextCursor::Down);
    }
    curseur.select(QTextCursor::LineUnderCursor);
    curseur.removeSelectedText();
    curseur.deleteChar();
    tableau->setTextCursor(curseur);
}

