<h1>Projet personnel - Gestionnaire de combats de jeu de rôles</h1>

<p><h2>Description du travail</h2>
Logiciel permettant la création d'un bestiaire et de combats pour un jeu de rôles.
Permet aussi la gestion du combat en lui-même, avec les attaques et le statut des différents participants.
</p>

<p><h2>Technologies et langages utilisés</h2>
<ul><li>Qt
    <li>C++
</ul></p>

<p><h2>Rôles dans le projet</h2>
<ul><li>Concepteur
    <li>Programmeur
</ul></p>

<p><h2>Participants</h2>
<ul><li>Ludovic Jozereau
</ul></p>

<p><h3>Pas d'exécutable disponible actuellement.</h3></p>