#include "GestionMort.h"

#include <QLayout>

GestionMort::GestionMort(std::vector<std::shared_ptr<Personnage> > &personnages, QWidget *parent) : QDialog(parent) {
    QGridLayout *layout = new QGridLayout();

    joueurs = recupJoueurs(personnages);

    zoneJoueurs = new QScrollArea();
    QGridLayout *scrollLayout = new QGridLayout();
    QWidget *scrollWidget = new QWidget(this);

    zoneJoueurs->setWidgetResizable(true);
    zoneJoueurs->setWidget(scrollWidget);
    layout->addWidget(zoneJoueurs, 0, 0);

    for (auto i = joueurs.begin(); i != joueurs.end(); ++i) {
        QLabel *nom = new QLabel((*i)->getNom(), this);
        listeJoueurs.push_back(nom);

        QGroupBox *groupeEtat = new QGroupBox(this);
        QHBoxLayout *layoutGroupe = new QHBoxLayout();
        QRadioButton *mort = new QRadioButton("Mort", groupeEtat);
        QRadioButton *vivant = new QRadioButton("Vivant", groupeEtat);

        layoutGroupe->addWidget(mort);
        layoutGroupe->addWidget(vivant);
        groupeEtat->setLayout(layoutGroupe);

        listeEtats.push_back(mort);
        listeEtats.push_back(vivant);
        listeGroupes.push_back(groupeEtat);

        if ((*i)->getVivant()) {
            vivant->setChecked(true);
        }
        else mort->setChecked(true);
    }

    for (int i = 0; i < listeJoueurs.size(); ++i) {
        scrollLayout->addWidget(listeJoueurs[i], i, 0);
        scrollLayout->addWidget(listeGroupes[i], i, 1);
    }

    scrollWidget->setLayout(scrollLayout);

    valider = new QPushButton("Valider les états");
    layout->addWidget(valider, 1, 1);

    this->setLayout(layout);

    QObject::connect(valider, SIGNAL(clicked()), this, SLOT(changerEtat()));
}

std::vector<std::shared_ptr<Personnage> > GestionMort::recupJoueurs(std::vector<std::shared_ptr<Personnage> > personnages) {
    std::vector<std::shared_ptr<Personnage> > joueurs = std::vector<std::shared_ptr<Personnage> >();

    for (auto i = personnages.begin(); i != personnages.end(); ++i) {
        if ((*i)->getType() == (int) constantes::type::JOUEUR) {
            joueurs.push_back(*i);
        }
    }

    return joueurs;
}

void GestionMort::changerEtat() {
    for (int i = 0; i < listeJoueurs.size(); ++i) {
        if (listeEtats[2*i]->isChecked()) joueurs[i]->setVivant(false);
        else joueurs[i]->setVivant(true);
    }

    this->close();
    emit affichage();
}
