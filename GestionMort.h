#ifndef GESTIONMORT_H
#define GESTIONMORT_H

#include "ressources/jdr/Personnage.h"

#include <QWidget>
#include <QDialog>
#include <QRadioButton>
#include <QPushButton>
#include <QLabel>
#include <QGroupBox>
#include <QScrollArea>
#include <memory>

class GestionMort : public QDialog {
    Q_OBJECT

private:
    QVector <QLabel *> listeJoueurs;
    QVector <QRadioButton *> listeEtats;
    QVector <QGroupBox *> listeGroupes;

    QPushButton * valider;

    std::vector<std::shared_ptr<Personnage> > joueurs;

    QScrollArea *zoneJoueurs;

public:
    GestionMort(std::vector<std::shared_ptr<Personnage> > &personnages, QWidget *parent = 0);

    std::vector<std::shared_ptr<Personnage> > recupJoueurs(std::vector<std::shared_ptr<Personnage> > personnages);

signals:
    void affichage();

public slots:
    void changerEtat();
};

#endif // GESTIONMORT_H
